package game;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.debug.DebugVectorRenderer;
import renderEngine.utils.Maths;
import renderEngine.utils.Quaternion;
import renderEngine.utils.Transform;
import renderEngine.utils.Transform2D;

public class Test {

//	private float time1 = 0;
//	private float time2 = 0;
	
	public static void main(String[] args) {
//		while(true) {
//			Random rand = new Random();
//			final float r = rand.nextFloat();
//			ArrayList<Transform2D> test1 = new ArrayList<Transform2D>();
//			
//			long begin = System.nanoTime();
//			for(int i = 0; i < 500; i++) {
//				test1.add(new Transform2D(new Vector2f(r, r), r, new Vector2f(r, r)));
//				for(int j = 1; j < i; j++) {
//					test1.get(j-1).multiply(test1.get(j));
//				}
//			}
//			System.out.println((System.nanoTime() - begin)/1000000000f);
//			
//			ArrayList<Transform> test2 = new ArrayList<Transform>();
//			begin = System.nanoTime();
//			for(int i = 0; i < 500; i++) {
//				Quaternion q = new Quaternion(new Vector3f(r, r, r), r);
//				Transform m = new Transform(q, new Vector3f(r, r, r), new Vector3f(r, r, r));
//				test2.add(m);
//				for(int j = 1; j < i; j++) {
//					Transform.multiply(test2.get(j-1), test2.get(j), test2.get(j-1));
//				}
//			}
//			System.out.println("3D: " + ((System.nanoTime() - begin)/1000000000f) + "\n");
//		}
		
		Matrix3f rand = Maths.createRotationMatrixRadians(new Vector3f(1,0,0.5f));
		System.out.println(rand);
		System.out.println(Matrix3f.transform(rand, new Vector3f(1,0,0), null));
		System.out.println(rand.m02);
	}
	
}
