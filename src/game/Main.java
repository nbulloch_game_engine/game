package game;

import org.lwjgl.opengl.Display;
import game.entities.blocks.Blocks;
import game.entities.blocks.techTree.Icons;
import game.entities.blocks.techTree.TechTree;
import game.states.ShowcaseState;
import game.states.buildState.BlockManager;
import game.states.buildState.BuildState;
import physicsEngine.physicsEngine.PhysicsManager;
import renderEngine.font.FontLoader;
import renderEngine.font.TextMaster;
import renderEngine.guis.GuiManager;
import renderEngine.postProcessing.PostProcessing;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;

public class Main {
	
	public static enum States{PLAYSTATE, BUILDSTATE, TESTSTATE, CLOSESTATE};
	private States currentState = States.BUILDSTATE;
	private BuildState buildState;
	private ShowcaseState showcaseState;
	private boolean showcase = false;
	
	public static void main(String[] args){
		DisplayManager.setIcon(new String[] {"icon128.png", "icon32.png", "icon16.png"});
		Main game = new Main();
		game.loop();
	}
	
	public void loop(){
		DisplayManager.createDisplay();
		Loader.init();
		GuiManager.init();
		PostProcessing.init();
		TextMaster.init(FontLoader.loadFont("res/DejaVuSansLightDistanceField.fnt"));
		Icons.loadIcons();
		Blocks.init();
		TechTree.createTree();
		BlockManager.init();
		PhysicsManager.init();
		if(showcase) {
			showcaseState = new ShowcaseState();
			while(currentState!=States.CLOSESTATE) {
				showcaseState.update();
				if(Display.isCloseRequested()){
					break;
				}
			}
		}else {
			while(currentState!=States.CLOSESTATE){
				switch(currentState){
				case BUILDSTATE:
					buildState = new BuildState();
					while(currentState==States.BUILDSTATE){
						if(!Display.isCloseRequested()){
							buildState.update();
						}else{
							currentState = States.CLOSESTATE;
						}
					}
					buildState.clean();
				default:
					break;
				}
			}
		}
		PostProcessing.clean();
		TextMaster.clean();
		Loader.clean();
		DisplayManager.closeDisplay();
	}
}
