package game.utils;

import renderEngine.renderEngine.Loader;
import renderEngine.textures.TerrainTexture;
import renderEngine.textures.TerrainTexturePack;

public class ResourceLoader {
	
	public static TerrainTexturePack loadTerrainTexturePack(String bgFile, String rFile, String gFile, String bFile){
		TerrainTexture backgroundTexture = new TerrainTexture(Loader.loadTexture(bgFile));
		TerrainTexture rTexture = new TerrainTexture(Loader.loadTexture(rFile));
		TerrainTexture gTexture = new TerrainTexture(Loader.loadTexture(gFile));
		TerrainTexture bTexture = new TerrainTexture(Loader.loadTexture(bFile));
		TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
		return texturePack;
	}
}