package game.states;

import java.util.ArrayList;

import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.Entity;
import renderEngine.entities.PointLight;
import renderEngine.entities.Scene;
import renderEngine.renderEngine.Loader;
import renderEngine.terrain.HeightsGenerator;
import renderEngine.terrain.Terrain;
import renderEngine.textures.TerrainTexture;

public class ShowcaseScene extends Scene{

	private static final Vector3f skyColor = new Vector3f(0.5f, 0.5f, 0.5f);
	private static final int MAX_SHADOW_MAP = 8192;
	private static final int MIN_SHADOW_MAP = 2048;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 95;
	private static final int FOV = 57;
	
	public static Vector4f clipPlane = new Vector4f();
	
	public ShowcaseScene(){
		staticNormalMapEntities = new ArrayList<Entity>();
		staticEntities = new ArrayList<Entity>();
		terrains = new ArrayList<Terrain>();
		lights = new ArrayList<PointLight>();
		DLights = new ArrayList<DirectionalLight>();
		Vector3f lightColor = new Vector3f(1f, 1f, 1f);
		sun = new DirectionalLight(new Vector3f(0.5f, -1f, 0.1f), lightColor);
		
		//terrain
		TerrainTexture texture = new TerrainTexture(Loader.loadTextureUnclamped("grass.png"));
		terrains.add(terrain = new Terrain(0, 0, texture, new HeightsGenerator(0,0,200,100), 200));
		
		float x = 50;
		float z = 50;
		Vector3f position = new Vector3f(x,terrain.getHeightOfTerrain(x, z)+1,z);
		lights.add(new PointLight(position, new Vector3f(1,.5f,.5f), new Vector3f(.1f,.1f,0f)));
		lights.add(new PointLight(new Vector3f(55, 8.87f, 54), new Vector3f(.1f, .1f, .2f), new Vector3f(0.1f,0.01f,.001f)));
		
		camera = new Camera(new Vector3f(50,10,50), Camera.NLERP);
		camera.setSpeed(10);
		csmShadows = true;
		shadows = false;
	}
	
	@Override
	public float getNearPlane() {
		return NEAR_PLANE;
	}
	
	@Override
	public float getFarPlane() {
		return FAR_PLANE;
	}
	
	@Override
	public int getNumCascades() {
		return 5;
	}
	
	@Override
	public Vector3f getSkyColor() {
		return skyColor;
	}
	
	@Override
	public Vector4f getClipPlane() {
		return clipPlane;
	}
	
	@Override
	public float getFogDensity() {
		return 0.03f;
	}
	
	@Override
	public float getFogGradient() {
		return 6f;
	}
	
	@Override
	public float getFogginess() {
		return 0.5f;
	}
	
	@Override
	public float getFOV() {
		return FOV;
	}
	
	@Override
	public int getMaxShadowSize() {
		return MAX_SHADOW_MAP;
	}
	
	@Override
	public int getMinShadowSize() {
		return MIN_SHADOW_MAP;
	}
	
}
