package game.states;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import game.states.buildState.BlockManager;
import renderEngine.debug.DebugVectorRenderer;
import renderEngine.entities.Entity;
import renderEngine.entities.Player;
import renderEngine.entities.PointLight;
import renderEngine.font.TextMaster;
import renderEngine.fontRenderer.FontRenderer;
import renderEngine.guiRenderer.GuiRenderer;
import renderEngine.guis.Button;
import renderEngine.models.TexturedModel;
import renderEngine.particles.ParticleMaster;
import renderEngine.particles.ParticleSystem;
import renderEngine.particles.ParticleTexture;
import renderEngine.postProcessing.Fbo;
import renderEngine.postProcessing.PostProcessing;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;
import renderEngine.renderEngine.MasterRenderer;
import renderEngine.utils.MouseManager;
import renderEngine.utils.MousePicker;
import renderEngine.utils.Quaternion;
import renderEngine.utils.ScreenShot;
import renderEngine.water.WaterFrameBuffers;
import renderEngine.water.WaterRenderer;
import renderEngine.water.WaterShader;
import renderEngine.water.WaterTile;

public class ShowcaseState implements GameState {
	
	private MasterRenderer renderer;
	private WaterRenderer waterRenderer;
	private MousePicker picker;
	private ArrayList<Entity> entities;
	private ArrayList<Entity> normalMapEntities;
	private Fbo MSFbo;
	private Fbo outputFbo;
	private Fbo outputFboBloom;
	
	private ShowcaseScene scene;
	private ArrayList<WaterTile> water;
	private WaterFrameBuffers buffers;
	private PointLight pointLight;
	private ParticleSystem fire, smoke;
	private Player player;
	private Entity crate;
	private Entity core;
	private int cnt = 0;
	
	private Quaternion[][][] array;
	private int size;
	
	public ShowcaseState(){
		entities = new ArrayList<Entity>();
		normalMapEntities = new ArrayList<Entity>();
		scene = new ShowcaseScene();
		renderer = new MasterRenderer(scene);
		
		ParticleMaster.init(renderer.getProjectionMatrix());
		
		buffers = new WaterFrameBuffers();
		waterRenderer = new WaterRenderer(new WaterShader(), renderer.getProjectionMatrix(), buffers);
		water = new ArrayList<WaterTile>();
		water.add(new WaterTile(25, 25, 5));
		pointLight = scene.getLights().get(0);
		
		picker = new MousePicker(scene.getCamera(), renderer.getProjectionMatrix(), scene.getTerrains().get(0));
		
		MSFbo = new Fbo(Display.getWidth(), Display.getHeight(), (byte)2);
		outputFbo = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		outputFboBloom = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		
		ParticleTexture smokeTexture = new ParticleTexture(Loader.loadTexture("smoke.png"), 8, false);
		smoke = new ParticleSystem(smokeTexture, 50, 2, 0, 3f, 2);
		smoke.setDirection(new Vector3f(0,1,0), .1f);
		smoke.setLifeError(.1f);
		smoke.setScaleError(.1f);
		smoke.setSpeedError(.1f);
		smoke.randomizeRotation();
		
		ParticleTexture fireTexture = new ParticleTexture(Loader.loadTexture("fire.png"), 8, false);
		fire = new ParticleSystem(fireTexture, 200, 2, 0, 1, 2);
		fire.setDirection(new Vector3f(0,1,0), .1f);
		fire.setLifeError(.1f);
		fire.setScaleError(.1f);
		fire.setSpeedError(.1f);
		fire.randomizeRotation();
		
		TexturedModel playerTM = new TexturedModel("showcase/bunny4", "showcase/bunnyTexture2.png", false);
		playerTM.setTranslucent(true);
		player = new Player(playerTM, new Vector3f(scene.getCamera().getPosition()), new Vector3f(), 3f);
		scene.getCamera().follow(player);
		
		Vector3f position = new Vector3f(scene.getLights().get(0).getPosition());
		position.x+=3;
		position.z+=1;
		position.y = scene.getTerrains().get(0).getHeightOfTerrain(position.x, position.z);
		crate = new Entity(new TexturedModel("showcase/Crate", "showcase/crate_diffuse_classic.png", true), position, new Vector3f(0,45,0), new Vector3f(), .6f);
		crate.getTexturedModel().getTexture().setNormalMap(Loader.loadTexture("showcase/crate_normal.png"));
		crate.getTexturedModel().getTexture().setSpecularMap(Loader.loadTexture("showcase/crate_specular_classic.png"));
		
		Vector3f position2 = new Vector3f(position);
		position2.x+=2;
		position2.z+=3;
		position2.y = scene.getTerrains().get(0).getHeightOfTerrain(position2.x, position2.z)+3;
		core = new Entity(new TexturedModel("core", "coreColor0.png", false), position2, new Vector3f(0,45,0), new Vector3f(1,1,1), 1f);
		core.getTexturedModel().getTexture().setLightMap(Loader.loadTexture("coreLightMap.png"));
		core.getTexturedModel().getTexture().setGlowMap(Loader.loadTexture("glowMap.png"));
		core.getTexturedModel().getTexture().setBloomWidth(1f);
		core.getTexturedModel().getTexture().setGlowMapFactor(.1f);
		core.getTexturedModel().getTexture().setGlowSpeed(1);
		
		scene.getStaticEntities().add(crate);
		scene.getStaticEntities().add(core);
		
		size = 10;
		array = new Quaternion[size][size][size];
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					array[i][j][k] = new Quaternion(new Vector3f(0,1,0),0);
				}
			}
		}
	}
	
	public void update() {
		MouseManager.updateCursor();
		Button.updatePressedButton();
		if(Keyboard.isKeyDown(Keyboard.KEY_F11))
			ScreenShot.render();
		if(Keyboard.isKeyDown(Keyboard.KEY_Y)){
			Vector3f target = new Vector3f(0,1,0);
			float time = 5;
			scene.getCamera().lookAtSmooth(target, time);
		}
		
		cnt++;
		float angle = cnt/20f;
		Quaternion rotation = new Quaternion(new Vector3f(1,0,0), angle);
		DebugVectorRenderer.clearLines();
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				for(int k = 0; k < size; k++) {
					Vector3f axis = new Vector3f(i-5f, j-5f, k-5.5f);
					axis.normalise();
					array[i][j][k] = new Quaternion(axis, angle);
					Quaternion product = Quaternion.multiply(rotation, array[i][j][k], null);
					product.normalize();
					DebugVectorRenderer.addLine(product, new Vector3f(i+40, j+7, 70+k));
				}
			}
		}
		
		Vector3f delta = new Vector3f((float)Math.sin((cnt)/360f),(float)Math.cos(cnt/360f),(float)Math.sqrt(2)/2);
		core.increaseRotation(delta);
		
		picker.update();
		scene.getCamera().update();
		BlockManager.update(scene.getCamera());
		
		player.move(scene.getTerrains().get(0));
		
		entities = new ArrayList<Entity>();
		entities.addAll(scene.getStaticEntities());
		entities.add(player);
		normalMapEntities = new ArrayList<Entity>();
		normalMapEntities.addAll(scene.getStaticNormalMapEntities());
		normalMapEntities.addAll(BlockManager.getEnities());
		
		Vector3f rand = new Vector3f(scene.getLights().get(0).getPosition());
		double theta = Math.random()*2*Math.PI;
		double r = Math.random()*.5f;
		rand.x+=Math.cos(theta)*r;
		rand.z+=Math.sin(theta)*r;
		fire.generateParticles(rand);
		smoke.generateParticles(scene.getLights().get(0).getPosition());
		ParticleMaster.update(scene.getCamera());
		
		renderer.updateCsm();
		render();
	}
	
	public void render(){
		float waterHeight = water.get(0).getHeight();
		
		GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
		//render reflection
		buffers.bindReflectionFrameBuffer();
		scene.getCamera().reflect(waterHeight);
		ShowcaseScene.clipPlane = new Vector4f(0,1,0,-waterHeight);
		renderer.renderScene(entities, normalMapEntities, scene.getTerrains());
		ParticleMaster.renderParticles(scene.getCamera());
		scene.getCamera().reflect(waterHeight);
		
		buffers.bindRefractionFrameBuffer();
		ShowcaseScene.clipPlane = new Vector4f(0,1,0,waterHeight);
		renderer.renderScene(entities, normalMapEntities, scene.getTerrains());
		ParticleMaster.renderParticles(scene.getCamera());
		GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
		buffers.unbindCurrentFrameBuffer();
		
		ShowcaseScene.clipPlane = new Vector4f();
		renderer.renderShadowMap(entities, normalMapEntities, scene.getSun());
		MSFbo.bindFrameBuffer();
		renderer.renderScene(entities, normalMapEntities, scene.getTerrains());
		waterRenderer.render(water, scene.getCamera(), pointLight, scene);
		ParticleMaster.renderParticles(scene.getCamera());
		MSFbo.unbindFrameBuffer();
		MSFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT0, outputFbo);
		MSFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT1, outputFboBloom);
		PostProcessing.doPostProcessing(outputFbo.getColorTexture(), outputFboBloom.getColorTexture());
		
		TextMaster.render();
		DisplayManager.updateDisplay();
	}
	
	public void clean(){
		PostProcessing.clean();
		renderer.clean();
		GuiRenderer.clean();
		FontRenderer.clean();
		ParticleMaster.clean();
		buffers.clean();
		MSFbo.clean();
		outputFbo.clean();
		outputFboBloom.clean();
	}
	
	@Override
	public void integrate() {}
}
