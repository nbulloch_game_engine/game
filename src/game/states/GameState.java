package game.states;

public interface GameState {
	
	public void update();
	public void integrate();
	public void render();
	
}
