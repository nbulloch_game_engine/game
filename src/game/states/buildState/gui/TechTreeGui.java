package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.Player;
import game.entities.blocks.techTree.Unlockable;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.guis.GuiState;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;

public class TechTreeGui extends GuiState{

	private TechTreeDisplay display;
	
	public static int[] textures = loadTextures();
	
	private DynamicText[] tokens;
	
	private static final int disabledButton = Loader.loadTexture("confirmButtonDisabled.png");
	private static final int purchaseButton = Loader.loadTexture("confirmButton.png");
	private static final int cancelButton = Loader.loadTexture("cancelButton.png");
	private static final int buttonHover = Loader.loadTexture("confirmButtonHover.png");
	private static boolean confirmUnlock = false;
	private static Unlockable unlockable;
	private GuiModel confirm;
	private GuiModel icon;
	private GuiModel token;
	private Button purchase;
	private Button cancel;
	private Text name;
	private Text cost;
	private boolean canUnlock;
	private FontSettings settings;
	
	public TechTreeGui() {
		display = new TechTreeDisplay(27, 27, this);
		
		GuiModel background = new GuiModel(new Vector2f(), new Vector2f(0.1f, 1), BuildGui.GREY, this);
		background.setLeftCenter(new Vector2f(-1, 0));
		
		tokens = new DynamicText[Player.TX+1];
		FontSettings tokenSettings = new FontSettings();
		tokenSettings.setRenderWidth(0.55f);
		tokenSettings.setFadeWidth(0.2f);
		
		float height = 0.06f;
		float scale = 0.95f/2;
		Vector2f size = new Vector2f(0, height*scale);
		float totalHeight = ((Player.TX + 1)*(height));
		
		GuiModel tokenBackground = new GuiModel(new Vector2f(), new Vector2f(1, totalHeight/2), new Vector4f(0.1f,0.1f,0.1f,1), background);
		tokenBackground.setLeftCenter(new Vector2f(-1, 0));
		
		size.y /= totalHeight/2;
		size.x = size.y / tokenBackground.getAspectRatio();
		float xOffset = -0.9f;
		
		for(int i = Player.T1; i <= Player.TX; i++){
			float y = 2*(1-(float)i/(Player.TX+1))-1;
			GuiPlacer.init(new Vector2f(), size, tokenBackground);
			GuiPlacer.setXRatio(1);
			GuiPlacer.setTopLeft(new Vector2f(xOffset, y));
			GuiModel token = new GuiModel(textures[i]);
			
			DynamicText text = new DynamicText(Player.getTokens(i) + "", new Vector2f(token.getRightCenter().x + 0.1f, y - height - 0.07f));
			text.setScale(0.4f);
			text.useSettings(tokenSettings);
			tokenBackground.addText(text);
			tokens[i] = text;
			
			if(Player.DEV_MODE)
				Player.addTokens(i, 999999);
			else
				Player.addTokens(i, 20);
		}
		
		Text techTokens = new Text("Tech Tokens", new Vector2f(0, 1.07f));
		techTokens.useSettings(BuildGui.getSmallFont());
		techTokens.setScale(0.3f);
		techTokens.center();
		tokenBackground.addText(techTokens);
	}
	
	private static int[] loadTextures(){
		textures = new int[Player.TX+1];
		String s = "techToken";
		textures[Player.T1] = Loader.loadTexture(s + "1.png");
		textures[Player.T2] = Loader.loadTexture(s + "2.png");
		textures[Player.T3] = Loader.loadTexture(s + "3.png");
		textures[Player.T4] = Loader.loadTexture(s + "4.png");
		textures[Player.T5] = Loader.loadTexture(s + "5.png");
		textures[Player.T6] = Loader.loadTexture(s + "6.png");
		textures[Player.T7] = Loader.loadTexture(s + "7.png");
		textures[Player.T8] = Loader.loadTexture(s + "8.png");
		textures[Player.T9] = Loader.loadTexture(s + "9.png");
		textures[Player.T10] = Loader.loadTexture(s + "10.png");
		textures[Player.TX] = Loader.loadTexture(s + "TX.png");
		return textures;
	}
	
	public static int getTokenTexture(int id){
		return textures[id];
	}
	
	public static void setConfirmPurchase(Unlockable unlockable){
		TechTreeGui.unlockable = unlockable;
		confirmUnlock = true;
	}
	
	@Override
	public void activate(){
		super.activate();
		display.reset();
		confirmUnlock = false;
		if(confirm!=null){
			confirm.disable();
		}
		MenuGui.guiControls.disable();
		MenuGui.guiTip.setText("UNLOCK NEW CUBES");
	}
	
	@Override
	public void update(){
		if(confirmUnlock){
			if(confirm == null){
				initConfirmGui();
			}else{
				if(!confirm.enabled()){
					confirm.enable();
					canUnlock = Player.getTokens(unlockable.getTokenLevel()) >= unlockable.getTokenCost();
					purchase.setTextures(
							canUnlock ? purchaseButton : disabledButton,
							buttonHover);
					token.switchTexture(textures[unlockable.getTokenLevel()]);
					icon.switchTexture(unlockable.getIcon());
					
					cost = new Text(unlockable.getTokenCost() + "", new Vector2f(-0.47f,-0.37f));
					cost.setScale(4);
					cost.useSettings(new FontSettings(settings));
					purchase.addText(cost);
					
					name = new Text(unlockable.getName(), new Vector2f(0.1f,0.1f));
					name.setScale(0.8f);
					name.center();
					name.useSettings(new FontSettings(settings));
					confirm.addText(name);
					
					purchase.update();
					
					setFocus(false);
					confirm.setFocus(true);
				}
				if(canUnlock)
					purchase.update();
				cancel.update();
				if(cancel.isActivated()){
					disableConfirm();
				}else if(purchase.isActivated()){
					disableConfirm();
					Player.payTokens(unlockable.getTokenLevel(), unlockable.getTokenCost());
					unlockable.purchase();
					display.unlockAdjacent(unlockable);
					setFocus(true);
				}
			}
			display.updateCells();
		}else{
			display.update();
		}
		for(int i = Player.T1; i <= Player.TX; i++){
			tokens[i].setText(Player.getTokens(i) + "");
		}
	}
	
	private void initConfirmGui(){
		confirm = new GuiModel(new Vector2f(), new Vector2f(0.27f,0.25f), Loader.loadTexture("confirmUnlock.png"), this);
		float aspect = confirm.getSize().x/confirm.getSize().y;
		
		purchase = new Button(new Vector2f(), new Vector2f(0.46f, 0.2f), confirm);
		purchase.setRightCenter(new Vector2f(-0.0175f, -0.4f));
		canUnlock = Player.getTokens(unlockable.getTokenLevel()) >= unlockable.getTokenCost();
		purchase.setTextures(
				canUnlock ? purchaseButton : disabledButton,
				buttonHover);
		purchase.setTextHoverColor(new Vector4f(0.3f,0.3f,0.3f,1));
		
		cancel = new Button(new Vector2f(), new Vector2f(0.46f, 0.2f), confirm);
		cancel.setLeftCenter(new Vector2f(0.0175f, -0.4f));
		cancel.setTextures(cancelButton, buttonHover);
		cancel.mirrorX();
		cancel.setTextHoverColor(new Vector4f(0.3f,0.3f,0.3f,1));
		
		settings = new FontSettings();
		settings.setRenderWidth(0.49f);
		settings.setFadeWidth(0.2f);
		
		Text cancelText = new Text("CANCEL", new Vector2f(0,0.1f));
		cancelText.center();
		cancelText.setScale(4);
		cancelText.useSettings(new FontSettings(settings));
		cancel.addText(cancelText);
		
		float size = 0.8f;
		float purchaseAspect = purchase.getSize().x/purchase.getSize().y;
		token = new GuiModel(new Vector2f(-0.76f,0.05f), new Vector2f(size/aspect/purchaseAspect/DisplayManager.getAspectRatio(), size), textures[unlockable.getTokenLevel()], purchase);
		
		cost = new Text(unlockable.getTokenCost() + "", new Vector2f(-0.47f,-0.37f));
		cost.setScale(4);
		cost.useSettings(new FontSettings(settings));
		purchase.addText(cost);
		
		icon = new GuiModel(new Vector2f(-0.8f,0.07f), new Vector2f(0.25f/aspect/DisplayManager.getAspectRatio(),0.25f), unlockable.getIcon(), confirm);
		
		name = new Text(unlockable.getName(), new Vector2f(0.1f,0.1f));
		name.setScale(0.8f);
		name.center();
		name.useSettings(new FontSettings(settings));
		confirm.addText(name);
		
		Text confirmText = new Text("//CONFIRM UNLOCK?", new Vector2f(-0.94f, 0.425f));
		confirmText.setScale(0.7f);
		confirmText.useSettings(settings);
		confirm.addText(confirmText);
	}
	
	private void disableConfirm(){
		confirmUnlock = false;
		confirm.disable();
		purchase.removeText(cost);
		confirm.removeText(name);
	}
}
