package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.blocks.techTree.TechTree;
import game.entities.blocks.techTree.Unlockable;
import game.entities.blocks.techTree.UnlockableItem;
import renderEngine.font.DynamicText;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.GuiComponent;
import renderEngine.guis.GuiModel;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;

public class TechTreeCell extends GuiComponent {

	private static final float HEIGHT = TechTreeDisplay.HEX_HEIGHT/2;
	private static final float SPACING = 0.9f;
	private static final int backgroundHex = Loader.loadTexture("hexBackground.png");
	private static final int lockedHex = Loader.loadTexture("hexLocked.png");
	private static final int unlockedHex = Loader.loadTexture("hexUnlocked.png");
	private static final int hexPurchaseButton = Loader.loadTexture("hexPurchaseButton.png");
	private static final int hexPurchaseButtonHover = Loader.loadTexture("hexPurchaseButtonHover.png");
	private static final int purchasedHex = Loader.loadTexture("hexPurchased.png");
	private static final int hexLock = Loader.loadTexture("hexLock.png");
	private static final Vector2f maxHotspot = new Vector2f(0.326f,-0.594f);
	private static final Vector2f minHotspot = new Vector2f(-0.326f,-0.934f);
	private static final float hotspotWidth = (float) (3/(4*Math.sqrt(3)));
	private static final Vector4f toolTipHotspot = new Vector4f(-hotspotWidth,-1, hotspotWidth, 1);
	private GuiModel overlay;
	private GuiModel lock;
	@SuppressWarnings("unused")
	private GuiModel icon, token;
	private UnlockableItem unlockable;
	private Button purchase;
	private Text name;
	private Text locked;
	private Text cost;
	
	public TechTreeCell(Vector2f position, GuiComponent parent) {
		super(position, new Vector2f(HEIGHT / DisplayManager.getAspectRatio() * 2 * SPACING, HEIGHT * SPACING), parent);
		new GuiModel(new Vector2f(), new Vector2f(1,1), backgroundHex, this);
	}
	
	public void setOverlayTexture(int texture){
		if(overlay==null){
			overlay = new GuiModel(new Vector2f(), new Vector2f(1.01f, 1.01f), texture, this);
		}else{
			overlay.setTexture(texture);
		}
	}
	
	public void setUnlockable(Unlockable unlockable){
		this.unlockable = (UnlockableItem) unlockable;
		overlay = new GuiModel(new Vector2f(), new Vector2f(1.01f, 1.01f), lockedHex, this);
		float scale = 0.5f;
		icon = new GuiModel(new Vector2f(0f, 0.4f), new Vector2f(0.5f*scale, 1f*scale), unlockable.getIcon(), this);
		lock = new GuiModel(new Vector2f(), new Vector2f(1,1), hexLock, this);
		
		locked = new Text("LOCKED", new Vector2f(0, -0.77f));
		locked.setScale(0.7f);
		locked.center();
		locked.useSettings(BuildGui.getSmallFont());
		addText(locked);
		
		name = new Text(unlockable.getName(), new Vector2f(0, -0.3f));
		name.setScale(0.7f);
		name.setLine(0.45f);
		name.center();
		name.useSettings(BuildGui.getSmallFont());
		addText(name);
	}
	
	public void addText(Text text){
		overlay.addText(text);
	}
	
	public void addText(DynamicText text){
		overlay.addText(text);
	}
	
	public void removeText(Text text){
		overlay.removeText(text);
	}
	
	public void removeText(DynamicText text){
		overlay.removeText(text);
	}

	public void update() {
		if(unlockable != null){
			if(unlockable.purchased()){
				if(purchase != null){
					overlay.removeChild(purchase);
					cost = null;
					token = null;
					purchase = null;
				}if(overlay.getTexture()!=purchasedHex){
					overlay.switchTexture(purchasedHex);
				}
			}else if(unlockable.unlocked()){
				if(purchase == null){
					purchase = new Button(new Vector2f(), new Vector2f(1f,1f), overlay);
					purchase.setMaxHotspot(maxHotspot);
					purchase.setMinHotspot(minHotspot);
					purchase.setTextures(hexPurchaseButton, hexPurchaseButtonHover);
					removeText(locked);
					locked = null;
					removeChild(lock);
					lock = null;
					
					float size = 0.15f;
					token = new GuiModel(new Vector2f(-0.23f, -0.77f), new Vector2f(size / DisplayManager.getAspectRatio(), size), TechTreeGui.getTokenTexture(unlockable.getTokenLevel()), purchase);
					
					cost = new Text(unlockable.getTokenCost() + "", new Vector2f(-0.1f, -0.83f));
					cost.setScale(0.8f);
					cost.useSettings(BuildGui.getSmallFont());
					cost.getSettings().setColor(new Vector4f(0.1f,0.1f,0.1f,1));
					purchase.addText(cost);
				}
				purchase.update();
				if(overlay.getTexture()!=unlockedHex){
					overlay.switchTexture(unlockedHex);
				}
			}
			if(purchase!=null){
				if(purchase.isActivated()){
					TechTreeGui.setConfirmPurchase(unlockable);
				}
			}
			if(unlockable.unlocked()) {
				unlockable.getItem().getInfoGui().update(this, toolTipHotspot);
			}
		}
		
	}

	public void unlock() {
		if(unlockable!=null)
			TechTree.unlock(unlockable);
	}
}
