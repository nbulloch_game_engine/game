package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import game.entities.blocks.BlockType;
import game.entities.blocks.Item;
import game.states.buildState.BuildState;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.DisplayCell;
import renderEngine.guis.GridDisplay;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;

public class ShopPurchaseCell extends DisplayCell {

	private static int cancelTexture = Loader.loadTexture("cancelPurchase.png");
	private static int cancelTexturePress = Loader.loadTexture("cancelPurchasePress.png");
	
	private Button cancel;
	private DynamicText numText;
	private DynamicText cost;
	private Item item;
	private FontSettings bold;
	private int numPurchasing = 0;
	private boolean credits;
	
	public ShopPurchaseCell(GridDisplay parent, Item item, boolean credits) {
		super(parent, credits ? item.getTabIndex() : item.getTabIndex() + BlockType.NUM_TYPES*BlockType.TIERS);
		this.item = item;
		this.credits = credits;
		GuiPlacer.init(new Vector2f(), new Vector2f(0.9f,0.9f), this);
		GuiPlacer.setXRatio(1);
		GuiPlacer.setLeftCenter(new Vector2f(-0.95f, 0));
		GuiModel icon = new GuiModel(item.getIcon());
		
		bold = new FontSettings();
		bold.setRenderWidth(0.55f);
		bold.setFadeWidth(0.3f);
		FontSettings nameSettings = new FontSettings();
		nameSettings.setRenderWidth(0.5f);
		nameSettings.setFadeWidth(0.2f);
		
		float xPos = icon.getRightCenter().x+0.08f;
		numText = new DynamicText("1", new Vector2f(xPos,0.5f));
		numText.setScale(1.5f);
		numText.useSettings(bold);
		addText(numText);
		
		Text name = new Text(item.getName(), new Vector2f(xPos,0));
		name.setScale(1.3f);
		name.useSettings(nameSettings);
		addText(name);
		
		float size = 0.3f;
		GuiPlacer.init(new Vector2f(), new Vector2f(size, size), this);
		GuiPlacer.setXRatio(1);
		GuiPlacer.setLeftCenter(new Vector2f(xPos-0.01f,-0.5f));
		GuiModel costIcon = new GuiModel(credits ? BuildState.creditTexture : BuildState.cubitTexture);
		
		cost = new DynamicText(item.getCost()+"", new Vector2f(costIcon.getRightCenter().x+0.05f, -0.6f));
		cost.setScale(1.35f);
		cost.useSettings(new FontSettings(bold));
		cost.getSettings().setColor(credits ? BuildGui.CREDIT_BLUE : BuildGui.CUBIT_GOLD);
		addText(cost);
		
		GuiPlacer.init(new Vector2f(), new Vector2f(1,0.25f), this);
		GuiPlacer.setXRatio(1);
		
		cancel = new Button();
		cancel.setTextures(cancelTexture, cancelTexturePress);
		float offset = 0.1f;
		float offsetX = offset/(getAspectRatio()*DisplayManager.getAspectRatio());
		cancel.setTopRight(new Vector2f(1-offsetX,1-offset));
	}
	
	public ShopPurchaseCell(GridDisplay parent) {
		super(parent, Integer.MAX_VALUE);
		removeChild(background);
		background = null;
	}
	
	public void purchaseDelta(int delta) {
		numPurchasing += delta;
		if(numPurchasing==0)
			ShopGui.removePurchase(this);
	}

	@Override
	public void update() {
		if(numText!=null) {
			int num = Math.abs(numPurchasing);
			numText.setText(num+"");
			String sign = numPurchasing<0 ? "+" : "";
			cost.setText(sign+num*item.getCost());
		}if(cancel!=null) {
			cancel.update();
			if(cancel.isActivated())
				ShopGui.removePurchase(this);
		}
	}
	
	public Item getItem() {
		return item;
	}
	
	public boolean credits() {
		return credits;
	}
	
	public int getNum() {
		return numPurchasing;
	}
}
