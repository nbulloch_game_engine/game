package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.blocks.Item;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.guis.ToolTip;
import renderEngine.renderEngine.Loader;

public class ItemInfo extends ToolTip{
	
	private static int texture = Loader.loadTexture("itemInfo.png");
	
	public ItemInfo(Item item) {
		super(new Vector2f(), new Vector2f(1,1), null);
		GuiPlacer.init(new Vector2f(), new Vector2f(1, 0.22f), this);
		GuiPlacer.setXRatio(2);
		GuiPlacer.setLowerLeft(new Vector2f(0,0));
		GuiModel info = new GuiModel(texture);
		
		GuiPlacer.init(new Vector2f(-0.67f, -0.18f), new Vector2f(1,0.5f), info);
		GuiPlacer.setXRatio(1);
		new GuiModel(item.getIcon());
		
		FontSettings settings = new FontSettings();
		settings.setRenderWidth(0.45f);
		settings.setFadeWidth(0.3f);
		settings.setColor(new Vector4f(0.8f, 0.8f, 0.8f, 1));
		
		Text text = new Text(item.getStats(), new Vector2f(-0.3f,0.5f));
		text.setScale(0.4f);
		text.useSettings(settings);
		info.addText(text);
		
		FontSettings bold = new FontSettings();
		bold.setRenderWidth(0.53f);
		bold.setFadeWidth(0.18f);
		
		Text name = new Text(item.getName(), new Vector2f(-0.9f, 0.74f));
		name.useSettings(bold);
		name.setScale(0.5f);
		info.addText(name);
	}

}
