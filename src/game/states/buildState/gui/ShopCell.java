package game.states.buildState.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.blocks.Item;
import game.states.buildState.BuildState;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.DisplayCell;
import renderEngine.guis.GridDisplay;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.renderEngine.Loader;

public class ShopCell extends DisplayCell {
	
	private static final int cubit = Loader.loadTexture("shopCubit.png");
	private static final int cubitPress = Loader.loadTexture("shopCubitPress.png");
	private static final int credit = Loader.loadTexture("shopCredit.png");
	private static final int creditPress = Loader.loadTexture("shopCreditPress.png");
	
	private Item item;
	private Button buyCredits;
	private Button buyCubits;
	private DynamicText owned;
	
	public ShopCell(Item item, GridDisplay parent) {
		super(parent, item.getTabIndex());
		setBackgroundColor(new Vector4f(0, 0, 0, 0.4f));
		this.item = item;
	}
	
	public void init(){
		float size = 0.75f;
		GuiPlacer.init(new Vector2f(), new Vector2f(size,size), this);
		GuiPlacer.setYRatio(1);
		GuiPlacer.setUpperCenter(new Vector2f(0,0.9f));
		new GuiModel(item.getIcon());
		
		Vector2f hoverSize = new Vector2f(1.025f, 1.025f);
		
		buyCubits = new Button(new Vector2f(0, -0.85f), new Vector2f(0.975f, 0.1f), this);
		buyCubits.setTextures(cubit, cubitPress, cubitPress);
		buyCubits.setHoverSize(hoverSize);
		buyCubits.setPressActivated();
		buyCubits.setRepeatDelay(30);
		
		buyCredits = new Button(new Vector2f(0, -0.625f), buyCubits.getSize(), this);
		buyCredits.setTextures(credit, creditPress, creditPress);
		buyCredits.setHoverSize(hoverSize);
		buyCredits.setPressActivated();
		buyCredits.setRepeatDelay(30);
		
		FontSettings thinFont = new FontSettings();
		thinFont.setRenderWidth(0.45f);
		thinFont.setFadeWidth(0.3f);
		
		Text name = new Text(item.getName(), new Vector2f(0, buyCredits.getTopCenter().y+0.1f));
		name.center();
		name.setScale(0.45f);
		name.useSettings(thinFont);
		addText(name);
		
		GuiPlacer.init(new Vector2f(), new Vector2f(1,1),  buyCredits);
		GuiPlacer.setXRatio(1);
		GuiPlacer.setLeftCenter(new Vector2f(-0.9f, 0));
		GuiModel creditIcon = new GuiModel(BuildState.creditTexture);
		
		new GuiModel(BuildState.cubitTexture, buyCubits);
		
		Vector2f costPos = new Vector2f(creditIcon.getRightCenter().x+0.05f, -0.55f);
		FontSettings boldFont = new FontSettings();
		boldFont.setRenderWidth(0.55f);
		boldFont.setFadeWidth(0.25f);
		
		Text cost = new Text(item.getCost() + "", costPos);
		cost.setScale(6f);
		cost.useSettings(boldFont);
		buyCredits.addText(cost);
		
		Text cubitCost = new Text(item.getCubitCost() + "", costPos);
		cubitCost.setScale(6f);
		cubitCost.useSettings(boldFont);
		buyCubits.addText(cubitCost);
		
		FontSettings ownedFont = new FontSettings(boldFont);
		ownedFont.setRenderWidth(0.57f);
		owned = new DynamicText(item.getNumOwned() + "", new Vector2f(-0.91f, 0.83f));
		owned.setScale(0.57f);
		owned.useSettings(ownedFont);
		addText(owned);
	}
	
	@Override
	public void update(){
		buyCredits.update();
		boolean sell = Keyboard.isKeyDown(Keyboard.KEY_S);
		if(!sell) {
			buyCubits.enableButton();
			buyCubits.update();
		}else {
			buyCubits.disableButton();
		}
		owned.setText(item.getNumOwned()+"");
		int num = 1;
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)||Keyboard.isKeyDown(Keyboard.KEY_RSHIFT))
			num = 25;
		if(buyCredits.isActivated()) {
			if(!sell)
				ShopGui.addPurchase(item, true, num);
			else
				ShopGui.sellItems(item, num);
		}if(buyCubits.isActivated())
			ShopGui.addPurchase(item, false, num);
		item.getInfoGui().update(this);
	}
	
	@Override
	public void disable() {
		super.disable();
	}
}