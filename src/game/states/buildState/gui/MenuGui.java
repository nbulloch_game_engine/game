package game.states.buildState.gui;

import java.util.HashMap;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.GuiManager;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiState;
import renderEngine.renderEngine.Loader;
import renderEngine.utils.MouseManager;

public class MenuGui extends GuiState{

	public static final int sideTexture = Loader.loadTexture("sideButton.png");
	public static final int sideTextureHover = Loader.loadTexture("sideButtonHover.png");
	private static final int centerTexture = Loader.loadTexture("centerButton.png");
	private static final int centerTextureHover = Loader.loadTexture("centerButtonHover.png");
	
	public static boolean closeGui = false;
	public static DynamicText guiTip;
	public static DynamicText guiControls;
		
	public static float minY = 1;
	
	private GuiModel topStrip;
	private GuiModel stripBorder;
	
	private HashMap<Integer, Button> buttons;
	
	private Button garage;
	private Button techTree;
	private Button battle;
	private Button shop;
	private Button inventory;
	
	private int activeState = BuildGui.BATTLE;
	
	public MenuGui(){
		topStrip = new GuiModel(new Vector2f(), new Vector2f(1, 0.052f), BuildGui.GREY, this);
		topStrip.setUpperCenter(new Vector2f(0, 1));
		
		stripBorder = new GuiModel(new Vector2f(), new Vector2f(1, 0.013f), new Vector4f(1, 1, 1, 1), this);
		stripBorder.setUpperCenter(new Vector2f(0, 1 - topStrip.getSize().y * 2));
		
		addChild(topStrip);
		
		minY = stripBorder.getLowerCenter().y;
		
		float sideButtonWidth = 0.07f;
		float sideFontScale = 3.1f;
		
		battle = new Button(new Vector2f(), new Vector2f(0.08f, 0.79f), topStrip);
		battle.setTextures(centerTexture, centerTextureHover);
		battle.setUpperCenter(new Vector2f(0, 0));
		
		Text battleText = new Text("BATTLE[E]", new Vector2f(0, 0.2f));
		battleText.center();
		battleText.setScale(2.5f);
		battle.setTextHoverColor(new Vector4f(0,0,0,1));
		battle.addText(battleText);
		
		FontSettings battleSettings = new FontSettings();
		battleSettings.setRenderWidth(0.5f);
		battleSettings.setFadeWidth(0.35f);
		battleText.useSettings(battleSettings);
		
		shop = new Button(new Vector2f(), new Vector2f(sideButtonWidth, 0.5f), topStrip);
		shop.setTextures(sideTexture, sideTextureHover);
		shop.setTopRight(new Vector2f(-battle.getSize().x, 0));
		
		Text shopText = new Text("CUBE DEPOT[C]", new Vector2f());
		shopText.center();
		shopText.setScale(sideFontScale);
		shop.setTextHoverColor(new Vector4f(0,0,0,1));
		shop.addText(shopText);
		
		FontSettings sideSettings = new FontSettings();
		sideSettings.setRenderWidth(0.55f);
		sideSettings.setFadeWidth(0.3f);
		shopText.useSettings(sideSettings);
		
		inventory = new Button(new Vector2f(), new Vector2f(sideButtonWidth, 0.5f), topStrip);
		inventory.setTextures(sideTexture, sideTextureHover);
		inventory.setTopRight(new Vector2f(-battle.getSize().x - sideButtonWidth * 2, 0));
		
		Text invText = new Text("INVENTORY[Q]", new Vector2f());
		invText.center();
		invText.setScale(sideFontScale);
		invText.useSettings(new FontSettings(sideSettings));
		inventory.setTextHoverColor(new Vector4f(0,0,0,1));
		inventory.addText(invText);
		
		techTree = new Button(new Vector2f(), new Vector2f(sideButtonWidth, 0.5f), topStrip);
		techTree.setTextures(sideTexture, sideTextureHover);
		techTree.setTopLeft(new Vector2f(battle.getSize().x, 0));
		techTree.mirrorX();
		
		Text techText = new Text("TECH TREE[T]", new Vector2f());
		techText.center();
		techText.setScale(sideFontScale);
		techText.useSettings(new FontSettings(sideSettings));
		techTree.setTextHoverColor(new Vector4f(0,0,0,1));
		techTree.addText(techText);
		
		garage = new Button(new Vector2f(), new Vector2f(sideButtonWidth, 0.5f), topStrip);
		garage.setTextures(sideTexture, sideTextureHover);
		garage.setTopLeft(new Vector2f(battle.getSize().x + sideButtonWidth * 2, 0));
		garage.mirrorX();
		
		Text garageText = new Text("GARAGE[G]", new Vector2f());
		garageText.center();
		garageText.setScale(sideFontScale);
		garageText.useSettings(new FontSettings(sideSettings));
		garage.setTextHoverColor(new Vector4f(0,0,0,1));
		garage.addText(garageText);
		
		animationTime = 200;
		animationPosition = new Vector2f(0, 1-stripBorder.getLowerCenter().y);
		animate = true;
		
		FontSettings tips = new FontSettings();
		tips.setColor(new Vector4f(0.7f, 0.7f, 0.7f, 1));
		tips.setRenderWidth(0.55f);
		tips.setFadeWidth(0.3f);
		
		guiTip = new DynamicText("", new Vector2f(-(battle.getSize().x + sideButtonWidth*2), 0.1f));
		guiTip.center();
		guiTip.useSettings(tips);
		guiTip.setScale(5f);
		stripBorder.addText(guiTip);
		
		guiControls = new DynamicText("", new Vector2f(battle.getSize().x + sideButtonWidth*2, 0.1f));
		guiControls.center();
		guiControls.useSettings(tips);
		guiControls.setScale(5f);
		stripBorder.addText(guiControls);
	}
	
	public void initButtons(){
		buttons = new HashMap<Integer, Button>();
		buttons.put(BuildGui.INVENTORY, inventory);
		buttons.put(BuildGui.SHOP, shop);
		buttons.put(BuildGui.BATTLE, battle);
		buttons.put(BuildGui.TECH_TREE, techTree);
		buttons.put(BuildGui.GARAGE, garage);
	}
	
	@Override
	public void activate(){
		super.activate();
		MouseManager.showMouse();
		battle.setSelected(true);
		GuiManager.getGuiState(BuildGui.BATTLE).activate();
		activeState = BuildGui.BATTLE;
		GuiManager.getGuiState(BuildGui.BUILD).deactivate();
	}
	
	@Override
	public void deactivate(){
		super.deactivate();
		GuiManager.getGuiState(activeState).deactivate();
	}
	
	@Override
	public void update(){
		super.update();
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)||closeGui){
			deselectButtons();
			GuiManager.getGuiState(BuildGui.BUILD).activate();
			deactivate();
			closeGui = false;
		}
		battle.update();
		shop.update();
		inventory.update();
		techTree.update();
		garage.update();
		if(shop.isActivated())
			switchState(BuildGui.SHOP);
		else if(inventory.isActivated())
			switchState(BuildGui.INVENTORY);
		else if(battle.isActivated())
			switchState(BuildGui.BATTLE);
		else if(techTree.isActivated())
			switchState(BuildGui.TECH_TREE);
		else if(garage.isActivated())
			switchState(BuildGui.GARAGE);
	}
	
	public void switchState(int newState){
		if(!active())
			activate();
		if(activeState!=newState){
			deselectButtons();
			GuiManager.getGuiState(activeState).deactivate();
			activeState = newState;
			GuiManager.getGuiState(activeState).activate();
			Button button = buttons.get(newState);
			if(button!=null)
				button.setSelected(true);
		}
	}
	
	private void deselectButtons(){
		battle.setSelected(false);
		shop.setSelected(false);
		inventory.setSelected(false);
		techTree.setSelected(false);
		garage.setSelected(false);
	}
}
