package game.states.buildState.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.Player;
import game.entities.blocks.BlockType;
import game.entities.blocks.Blocks;
import game.entities.blocks.Item;
import game.states.buildState.BuildState;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.GridDisplay;
import renderEngine.guis.GuiComponent;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.guis.GuiState;
import renderEngine.guis.ScrollBar;
import renderEngine.renderEngine.Loader;

public class ShopGui extends GuiState{
	
	public static int NUM_CELLS = 0;
	
	private static final int buyButton = Loader.loadTexture("buyItems.png");
	private static final int buyButtonPressed = Loader.loadTexture("buyItemsPressed.png");
	private static final int buyButtonDisabled = Loader.loadTexture("buyItemsDisabled.png");
	private static final int confirmTexture = Loader.loadTexture("shopConfirm.png");
	private static final int itemIcon = Loader.loadTexture("cubeIconWhite.png");
	
	private static HashMap<Integer, ArrayList<Item>> prepurchased = new HashMap<Integer, ArrayList<Item>>();
	
	private static HashMap<Item, ShopPurchaseCell[]> purchaseList = new HashMap<Item, ShopPurchaseCell[]>();
	
	private static GridDisplay[] shopTabs;
	private static GridDisplay purchaseSummary;
	private static int totalCredits = 0;
	private static int totalCubits = 0;
	private static int numCredits = 0;
	private static int numCubits = 0;
	private static Button buyItems;
	private static ShopPurchaseCell cost;
	private static GuiComponent credits, cubits;
	private GuiModel buyItemIcon;
	private GuiModel cubitIcon;
	private GuiModel creditIcon;
	private DynamicText cubitTotal;
	private DynamicText creditTotal;
	private FontSettings buySettings, cubitSettings, creditSettings;
	private GridDisplay active;
	
	private GuiModel confirm;
	private Button confirmButton;
	private Button cancelButton;
	private DynamicText confirmCredits, confirmCubits, creditCubes, cubitCubes;
	
	public ShopGui(){
		super(new Vector2f(), new Vector2f(1, 1-(1-MenuGui.minY)/2));
		setLowerCenter(new Vector2f(0, -1));
		
		GuiPlacer.init(new Vector2f(), new Vector2f(0.125f,1),  this);
		GuiPlacer.setYRatio(0.0625f);
		GuiPlacer.setLowerLeft(new Vector2f(-1, -1));
		GuiPlacer.setLowerRight(new Vector2f(1, -1));
		float center = GuiPlacer.getSize().y;
		GuiPlacer.init(new Vector2f(), new Vector2f(0.125f*0.9375f, 1-(GuiPlacer.getSize().y+0.005f)*2), this);
		GuiPlacer.setLeftCenter(new Vector2f(-1, center));
		GuiPlacer.setRightCenter(new Vector2f(1, center));
		GuiModel rightPanel = new GuiModel(BuildGui.GREY);
		
		Vector2f panelSize = GuiPlacer.getSize();
		Vector2f panelPos = GuiPlacer.getPos();
		
		Vector2f barSize = new Vector2f(rightPanel.getSize());
		barSize.x = 0.005f;
		Vector2f barPos = new Vector2f(rightPanel.getLeftCenter());
		barPos.x-=barSize.x + 0.002f;
		shopTabs = new GridDisplay[TabsGui.NUM_TABS];
		for(int i = 0; i < shopTabs.length; i++){
			ScrollBar bar = new ScrollBar(barPos, barSize, 0, this);
			bar.setInputColor(BuildGui.ORANGE);
			bar.setTrackColor(BuildGui.LIGHT_GREY);
			shopTabs[i] = new GridDisplay(new Vector2f(0,panelPos.y), new Vector2f(0.75f, panelSize.y), 9, bar, 1.25f, this);
			shopTabs[i].setBackground(new Vector4f(0,0,0,0));
			shopTabs[i].disable();
			shopTabs[i].showAll();
		}
		active = shopTabs[TabsGui.getActiveTab()];
		active.enable();
		
		for(Integer tab : prepurchased.keySet()){
			for(Item item : prepurchased.get(tab)){
				ShopCell cell = (ShopCell) new ShopCell(item, shopTabs[tab]).setIndex(item.getTabIndex());
				cell.init();
			}
		}prepurchased = null;
		
		GuiPlacer.init(new Vector2f(), new Vector2f(0.95f, 1), rightPanel);
		GuiPlacer.setYRatio(0.25f);
		GuiPlacer.setLowerCenter(new Vector2f(0, -1.019f));
		buyItems = new Button();
		buyItems.setTextures(buyButton, buyButtonPressed);
		buyItems.setDisabledTexture(buyButtonDisabled);
		buyItems.setMinHotspot(new Vector2f(-1, -0.56f));
		
		GuiPlacer.init(new Vector2f(), new Vector2f(1, 0.6f), buyItems);
		GuiPlacer.setXRatio(1);
		GuiPlacer.setLeftCenter(new Vector2f(-0.85f, 0.25f));
		buyItemIcon = new GuiModel(TabsGui.ICONS[1][0]);
		
		float pad = 0.01f;
		float padX = pad/rightPanel.getAspectRatio()/2;
		ScrollBar scrollBar = new ScrollBar(new Vector2f(), new Vector2f(0.03f, 0.5f-buyItems.getTopCenter().y/2-pad), 0, rightPanel);
		scrollBar.setTopRight(new Vector2f(1-padX, 1-pad));
		scrollBar.setInputColor(BuildGui.ORANGE);
		scrollBar.setTrackColor(BuildGui.LIGHT_GREY);
		GridDisplay.hasBackground = false;
		purchaseSummary = new GridDisplay(new Vector2f(), new Vector2f(1-(padX+scrollBar.getSize().x/2), 0.5f-scrollBar.getLowerCenter().y/2), 1, scrollBar, 0.35f, rightPanel);
		purchaseSummary.setTopLeft(new Vector2f(-1,1));
		
		buySettings = new FontSettings();
		buySettings.setColor(BuildGui.LIGHT_GREY);
		buySettings.setRenderWidth(0.6f);
		buySettings.setFadeWidth(0.2f);
		
		Text buyText = new Text("BUY CUBES", new Vector2f(-0.45f, 0.1f));
		buyText.useSettings(buySettings);
		buyText.setScale(2f);
		buyItems.addText(buyText);
		buyItems.update();
		
		initTotal();
		
		initConfirm();
		
		if(Player.DEV_MODE) {
			HashMap<Item, Integer> items = new HashMap<Item, Integer>();
			Iterator<BlockType> i = Blocks.blocks.iterator();
			while(i.hasNext()) {
				BlockType block = i.next();
				for(Item item : block.getItems()) {
					if(item.getTabIndex()!=-1)
						items.put(item, 100);
				}
			}
			InventoryGui.addItems(items);
		}
		
		update();
	}
	
	@Override
	public void update(){
		super.update();
		if(TabsGui.updated()) {
			active.disable();
			active = shopTabs[TabsGui.getActiveTab()];
			active.enable();
		}
		if(buyItems.isActivated()) {
			buyItemIcon.switchTexture(itemIcon);
			buySettings.setColor(new Vector4f(1,1,1,1));
			confirm.enable();
			setFocus(false);
			confirm.setFocus(true);
		}
		buyItems.update();
		if(confirm.enabled()) {
			updateConfirm();
		}
		shopTabs[TabsGui.getActiveTab()].update();
		purchaseSummary.update();
		updateTotal();
	}
	
	private void updateConfirm() {
		confirmButton.update();
		cancelButton.update();
		String sign = totalCredits<0 ? "+" : "";
		confirmCredits.setText(sign+Math.abs(totalCredits));
		confirmCubits.setText(""+totalCubits);
		creditCubes.setText(""+numCredits);
		cubitCubes.setText(""+numCubits);
		if(cancelButton.isActivated()) {
			confirm.disable();
			setFocus(true);
		}if(confirmButton.isActivated()) {
			MenuGui.closeGui = true;
			Player.payCredits(totalCredits);
			Player.payCubits(totalCubits);
			HashMap<Item, Integer> inventory = new HashMap<Item, Integer>();
			for(Item purchase : purchaseList.keySet()) {
				ShopPurchaseCell[] cells = purchaseList.get(purchase);
				int num = 0;
				if(cells[0]!=null) {
					num+=cells[0].getNum();
					cells[0].remove();
				}if(cells[1]!=null) {
					num+=cells[1].getNum();
					cells[1].remove();
				}
				inventory.put(purchase, num);
			}InventoryGui.addItems(inventory);
			purchaseList.clear();
			totalCredits = 0;
			totalCubits = 0;
			numCredits = 0;
			numCubits = 0;
			credits.disable();
			cubits.disable();
		}
	}
	
	@Override
	public void activate() {
		super.activate();
		MenuGui.guiTip.setText("SPEND YOUR CREDITS HERE");
		MenuGui.guiControls.setText("x25[SHIFT]__SELL[S]");
		buyItemIcon.switchTexture(TabsGui.ICONS[1][0]);
		buySettings.setColor(BuildGui.LIGHT_GREY);
		MenuGui.guiControls.enable();
		if(shopTabs!=null) {
			active.disable();
			active = shopTabs[TabsGui.getActiveTab()];
			active.enable();
		}
	}
	
	@Override
	public void deactivate() {
		super.deactivate();
		confirm.disable();
		MenuGui.guiControls.disable();
	}
	
	private void initConfirm() {
		GuiPlacer.initWorldCoords(new Vector2f(), new Vector2f(0.23f, 0), this);
		GuiPlacer.setYRatio(0.5f);
		confirm = new GuiModel(confirmTexture);
		
		FontSettings thin = new FontSettings();
		thin.setRenderWidth(0.4f);
		thin.setFadeWidth(0.3f);
		
		Text slash = new Text("//", new Vector2f(-0.925f, 0.725f));
		slash.useSettings(thin);
		slash.setScale(0.6f);
		confirm.addText(slash);
		
		FontSettings header = new FontSettings();
		header.setRenderWidth(0.5f);
		header.setFadeWidth(0.3f);
		
		Text confirmPurchase = new Text("CONFIRM PURCHASE", new Vector2f(-0.8f, 0.725f));
		confirmPurchase.useSettings(header);
		confirmPurchase.setScale(0.5f);
		confirm.addText(confirmPurchase);
		
		float creditY = 0.3f;
		float cubitY = -0.05f;
		
		Text blueItems = new Text("CUBES", new Vector2f(-0.35f, creditY));
		blueItems.setScale(0.5f);
		blueItems.useSettings(creditSettings);
		confirm.addText(blueItems);
		
		creditCubes = new DynamicText("0", new Vector2f(-0.4f, creditY));
		creditCubes.alignRight();
		creditCubes.useSettings(creditSettings);
		creditCubes.setScale(0.5f);
		confirm.addText(creditCubes);
		
		confirmCredits = new DynamicText("0", new Vector2f(0.1f, creditY));
		confirmCredits.useSettings(creditSettings);
		confirmCredits.setScale(0.5f);
		confirm.addText(confirmCredits);
		
		Text goldItems = new Text("CUBES", new Vector2f(-0.35f, cubitY));
		goldItems.setScale(0.5f);
		goldItems.useSettings(cubitSettings);
		confirm.addText(goldItems);
		
		cubitCubes = new DynamicText("0", new Vector2f(-0.4f, cubitY));
		cubitCubes.alignRight();
		cubitCubes.useSettings(cubitSettings);
		cubitCubes.setScale(0.5f);
		confirm.addText(cubitCubes);
		
		confirmCubits = new DynamicText("0", new Vector2f(0.1f, cubitY));
		confirmCubits.setScale(0.5f);
		confirmCubits.useSettings(cubitSettings);
		confirm.addText(confirmCubits);
		
		GuiPlacer.init(new Vector2f(0,creditY-0.01f), new Vector2f(1, 0.125f), confirm);
		GuiPlacer.setXRatio(1);
		new GuiModel(BuildState.creditTexture);
		
		GuiPlacer.init(new Vector2f(0,cubitY-0.01f), new Vector2f(1, 0.125f), confirm);
		GuiPlacer.setXRatio(1);
		new GuiModel(BuildState.cubitTexture);
		
		FontSettings settings = new FontSettings();
		settings.setRenderWidth(0.5f);
		settings.setFadeWidth(0.3f);
		
		GuiPlacer.init(new Vector2f(), new Vector2f(0.47f, 1), confirm);
		GuiPlacer.setYRatio(0.25f);
		GuiPlacer.setLowerLeft(new Vector2f(-0.955f, -0.9f));
		confirmButton = new Button();
		confirmButton.setTextures(MenuGui.sideTexture, MenuGui.sideTextureHover);
		confirmButton.mirrorX();
		confirmButton.setTextHoverColor(new Vector4f(0,0,0,1));
		
		Text confirmText = new Text("CONFIRM", new Vector2f(0, 0.1f));
		confirmText.setScale(3f);
		confirmText.useSettings(settings);
		confirmText.center();
		confirmButton.addText(confirmText);
		
		GuiPlacer.init(new Vector2f(), new Vector2f(0.47f, 1), confirm);
		GuiPlacer.setYRatio(0.25f);
		GuiPlacer.setLowerRight(new Vector2f(0.955f, -0.9f));
		cancelButton = new Button();
		cancelButton.setTextures(MenuGui.sideTexture, MenuGui.sideTextureHover);
		cancelButton.setTextHoverColor(new Vector4f(0,0,0,1));
		
		Text cancelText = new Text("CANCEL", new Vector2f(0, 0.1f));
		cancelText.setScale(3f);
		cancelText.useSettings(new FontSettings(settings));
		cancelText.center();
		cancelButton.addText(cancelText);
		
		confirm.disable();
	}
	
	private void initTotal() {
		cost = new ShopPurchaseCell(purchaseSummary);
		
		creditSettings = new FontSettings();
		creditSettings.setColor(BuildGui.CREDIT_BLUE);
		creditSettings.setRenderWidth(0.45f);
		creditSettings.setFadeWidth(0.4f);
		creditSettings.useBasePosition(false);
		
		credits = new GuiComponent(new Vector2f(), new Vector2f(1,1), cost);
		
		Text totalBlue = new Text("TOTAL", new Vector2f(-0.9f,0));
		totalBlue.setScale(1.5f);
		totalBlue.useSettings(creditSettings);
		credits.addText(totalBlue);
		
		GuiPlacer.init(new Vector2f(-0.25f, 0), new Vector2f(1, 0.35f), credits);
		GuiPlacer.setXRatio(1);
		creditIcon = new GuiModel(BuildState.creditTexture); 
		
		creditTotal = new DynamicText(""+totalCredits, new Vector2f(creditIcon.getRightCenter().x+0.08f, 0));
		creditTotal.setScale(1.5f);
		creditTotal.useSettings(creditSettings);
		credits.addText(creditTotal);
		
		cubitSettings = new FontSettings(creditSettings);
		cubitSettings.setColor(BuildGui.CUBIT_GOLD);
		
		cubits = new GuiComponent(new Vector2f(), new Vector2f(1,1), cost);
		
		Text totalGold = new Text("TOTAL", new Vector2f(-0.9f,0));
		totalGold.setScale(1.5f);
		totalGold.useSettings(cubitSettings);
		cubits.addText(totalGold);
		
		GuiPlacer.init(new Vector2f(-0.25f, 0), new Vector2f(1, 0.35f), cubits);
		GuiPlacer.setXRatio(1);
		cubitIcon = new GuiModel(BuildState.cubitTexture); 
		
		cubitTotal = new DynamicText(""+totalCubits, new Vector2f(cubitIcon.getRightCenter().x+0.08f, 0));
		cubitTotal.setScale(1.5f);
		cubitTotal.useSettings(cubitSettings);
		cubits.addText(cubitTotal);
		
		buyItems.disableButton();
		credits.disable();
		cubits.disable();
	}
	
	private void updateTotal() {
		if(numCredits!=0) {
			String sign = totalCredits<0 ? "+" : "";
			creditTotal.setText(sign+Math.abs(totalCredits));
		}if(numCubits!=0) {
			cubitTotal.setText(""+totalCubits);
		}
	}
	
	public static void addItem(Item item){
		if(shopTabs!=null){
			ShopCell cell = (ShopCell) new ShopCell(item, shopTabs[item.getTab()]).setIndex(item.getTabIndex());
			cell.init();
		}else{
			ArrayList<Item> batch = prepurchased.get(item.getTab());
			if(batch==null){
				batch = new ArrayList<Item>();
				batch.add(item);
				prepurchased.put(item.getTab(), batch);
			}else{
				prepurchased.get(item.getTab()).add(item);
			}
		}
		NUM_CELLS++;
	}
	
	public static void addPurchase(Item item, boolean credits, int amt){
		if(credits ? item.getCost()*amt+totalCredits<=Player.getCredits() : item.getCubitCost()*amt+totalCubits<=Player.getCubits()) {
			ShopPurchaseCell[] batch = purchaseList.get(item);
			int index = credits ? 0 : 1;
			if(batch==null){
				batch = new ShopPurchaseCell[2];	
				purchaseList.put(item, batch);
			}if(batch[index]==null){
				batch[index] = new ShopPurchaseCell(purchaseSummary, item, credits);
			}batch[index].purchaseDelta(amt);
			if(credits) {
				totalCredits += item.getCost()*amt;
				numCredits+=amt;
			}else {
				totalCubits += item.getCubitCost()*amt;
				numCubits+=amt;
			}
			if(credits&&!ShopGui.credits.enabled()) {
				if(!cubits.enabled()) {
					ShopGui.credits.setPosition(new Vector2f());
				}else {
					ShopGui.credits.setPosition(new Vector2f(0,0.4f));
					cubits.setPosition(new Vector2f(0,-0.4f));
				}ShopGui.credits.enable();
			}else if(!(credits||cubits.enabled())){
				if(!ShopGui.credits.enabled()) {
					cubits.setPosition(new Vector2f());
				}else {
					ShopGui.credits.setPosition(new Vector2f(0,0.4f));
					cubits.setPosition(new Vector2f(0,-0.4f));
				}cubits.enable();
			}
			if(numCredits!=0||numCubits!=0) {
				buyItems.enableButton();
			}
		}
	}
	
	public static void sellItems(Item item, int amt) {
		int totalItems = item.getNumOwned();
		if(purchaseList.get(item)!=null&&purchaseList.get(item)[0]!=null)
			totalItems += purchaseList.get(item)[0].getNum();
		if(totalItems<amt) {
			amt = totalItems;
		}if(amt!=0) {
			ShopPurchaseCell[] batch = purchaseList.get(item);
			if(batch==null){
				batch = new ShopPurchaseCell[2];	
				purchaseList.put(item, batch);
			}if(batch[0]==null){
				batch[0] = new ShopPurchaseCell(purchaseSummary, item, true);
			}
			totalCredits-=item.getCost()*amt;
			numCredits-=amt;
			purchaseList.get(item)[0].purchaseDelta(-amt);
		}
		if(!credits.enabled()) {
			if(!cubits.enabled()) {
				credits.setPosition(new Vector2f());
			}else {
				credits.setPosition(new Vector2f(0,0.4f));
				cubits.setPosition(new Vector2f(0,-0.4f));
			}credits.enable();
		}
		if(numCredits==0) {
			credits.disable();
			cubits.setPosition(new Vector2f());
		}
	}
	
	public static void removePurchase(ShopPurchaseCell purchase) {
		Item item = purchase.getItem();
		if(purchase.credits()) {
			totalCredits -= item.getCost()*purchase.getNum();
			numCredits -= purchase.getNum();
			purchaseList.get(item)[0] = null;
		}else {
			totalCubits -= item.getCubitCost()*purchase.getNum();
			numCubits -= purchase.getNum();
			purchaseList.get(item)[1] = null;
		}purchase.remove();
		if(numCredits==0) {
			credits.disable();
			cubits.setPosition(new Vector2f());
		}if(numCubits==0) {
			cubits.disable();
			credits.setPosition(new Vector2f());
		}
		if(numCredits!=0||numCubits!=0) {
			buyItems.enableButton();
		}
	}
	
	public void confirmPurchase() {
		purchaseList.clear();
		buyItems.disableButton();
		credits.disable();
		cubits.disable();
	}
}
