package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.blocks.Item;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.DisplayCell;
import renderEngine.guis.GridDisplay;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.renderEngine.Loader;

public class InventoryCell extends DisplayCell{
	
	private static final int cellTexture = Loader.loadTexture("inventoryCell.png");
	private static final int cellHover = Loader.loadTexture("inventoryCellHover.png");
	
	private static Button selected;
	
	private Item item;
	private Button select;
	private DynamicText owned;
	
	public InventoryCell(Item item, GridDisplay parent) {
		super(parent, item.getTabIndex());
		setBackgroundColor(new Vector4f(0, 0, 0, 0.4f));
		removeChild(background);
		select = new Button(new Vector2f(), new Vector2f(1,1), this);
		select.setTextures(cellTexture, cellHover);
		select.getButtonModel().setSize(new Vector2f(1, 1.6f));
		select.getButtonModel().setTopLeft(new Vector2f(-1,1));
		select.setTextHoverColor(new Vector4f(0,0,0,1));
		select.setPressActivated();
		this.item = item;
		init();
	}
	
	private void init(){
		float size = 0.8f;
		GuiPlacer.init(new Vector2f(), new Vector2f(size,size), this);
		GuiPlacer.setYRatio(1);
		GuiPlacer.setUpperCenter(new Vector2f(0,0.67f));
		new GuiModel(item.getIcon());
		
		FontSettings thinFont = new FontSettings();
		thinFont.setRenderWidth(0.49f);
		thinFont.setFadeWidth(0.23f);
		thinFont.setColor(new Vector4f(0.7f,0.7f,0.7f,1));
		
		Text name = new Text(item.getName(), new Vector2f(-0.9f,-0.92f));
		name.setScale(0.6f);
		name.useSettings(thinFont);
		addText(name);
		
		FontSettings boldFont = new FontSettings();
		boldFont.setRenderWidth(0.55f);
		boldFont.setFadeWidth(0.25f);
		
		FontSettings ownedFont = new FontSettings(boldFont);
		ownedFont.setRenderWidth(0.57f);
		owned = new DynamicText(item.getNumOwned() + "", new Vector2f(-0.9f, 0.8f));
		owned.setScale(0.6f);
		owned.useSettings(ownedFont);
		select.addText(owned);
	}
	
	@Override
	public void update(){
		select.update();
		if(select.isActivated()) {
			InventoryGui.select(item);
			if(selected!=null){
				selected.setSelected(false);
			}
			selected = select;
			selected.setSelected(true);
		}
		owned.setText(item.getNumOwned()+"");
		item.getInfoGui().update(this);
	}
}
