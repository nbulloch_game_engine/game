package game.states.buildState.gui;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.Player;
import game.entities.blocks.BlockType;
import game.entities.blocks.Blocks;
import game.entities.blocks.Item;
import game.states.buildState.BuildState;
import renderEngine.font.DynamicText;
import renderEngine.font.FontSettings;
import renderEngine.guis.Gui;
import renderEngine.guis.GuiManager;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.postProcessing.PostProcessing;

public class BuildGui extends Gui {
	
	public static int BUILD, BATTLE, SHOP, INVENTORY, TECH_TREE, GARAGE, MENU;
	public static final Vector4f GREY = new Vector4f(0.18f, 0.18f, 0.18f, 1);
	public static final Vector4f LIGHT_GREY = new Vector4f(0.25f, 0.25f, 0.25f, 1);
	public static final Vector4f ORANGE = new Vector4f(1, .557f, .031f, 1);
	public static final Vector4f CREDIT_BLUE = new Vector4f(0.224f, 0.741f, 0.91f, 1);
	public static final Vector4f CUBIT_GOLD = new Vector4f(1f, 0.95f, 0.3f, 1);
	public static final Vector4f MONEY_GREEN = new Vector4f(0.2f, 0.858f, 0.541f, 1);
	
	private static final Vector2f position = new Vector2f();
	private static final Vector2f size = new Vector2f(1f, 1f);
	
	private static FontSettings defaultSmallFont;
	
	private ArrayList<ItemInfo> itemTips;
	private GuiModel displayBox;
	private GuiModel darken;
	private DynamicText credits;
	private DynamicText cubits;
	private DynamicText tokens;
	private int tokenLevel = Player.T1;
	
	public BuildGui() {
		super(position, size);
		
		defaultSmallFont = new FontSettings();
		defaultSmallFont.setRenderWidth(0.5f);
		defaultSmallFont.setFadeWidth(0.3f);
		
		BuildGuiState buildStateGui = new BuildGuiState();
		MenuGui menuGui = new MenuGui();
		TabsGui tabsGui = new TabsGui();
		BattleGui battleGui = new BattleGui();
		ShopGui shopGui = new ShopGui();
		InventoryGui invGui = new InventoryGui();
		TechTreeGui techTreeGui = new TechTreeGui();
		GarageGui garageGui = new GarageGui();
		addState(buildStateGui);
		addState(battleGui);
		addState(shopGui);
		addState(invGui);
		addState(techTreeGui);
		addState(garageGui);
		addState(menuGui);
		shopGui.addSubState(tabsGui);
		invGui.addSubState(tabsGui);
		buildStateGui.activate();
		
		BUILD = buildStateGui.getID();
		MENU = menuGui.getID();
		BATTLE = battleGui.getID();
		SHOP = shopGui.getID();
		INVENTORY = invGui.getID();
		TECH_TREE = techTreeGui.getID();
		GARAGE = garageGui.getID();
		
		menuGui.initButtons();
		
		displayBox = new GuiModel(new Vector2f(), new Vector2f(0.15f, 0.02f), GREY, this);
		displayBox.setTopRight(new Vector2f(0.99f, 0.99f));
		topChildren.add(displayBox);
		
		float textScale = 3.5f;
		float textHeight = -0.35f;
		
		GuiPlacer.init(new Vector2f(), new Vector2f(1,1), displayBox);
		GuiPlacer.setXRatio(1);
		GuiPlacer.setLeftCenter(new Vector2f(-0.99f,0));
		GuiModel tokenTexture = new GuiModel(TechTreeGui.getTokenTexture(tokenLevel));//dynamic level from bot
		topChildren.add(tokenTexture);
		
		tokens = new DynamicText(Player.getTokens(tokenLevel) + "", new Vector2f(tokenTexture.getRightCenter().x + 0.02f, textHeight));
		tokens.useSettings(getSmallFont());
		tokens.setScale(textScale);
		tokens.getSettings().setColor(new Vector4f(0f, 0.45f, 0.9f, 1));
		displayBox.addText(tokens);
		
		GuiModel creditTexture = new GuiModel(new Vector2f(), tokenTexture.getSize(), BuildState.creditTexture, displayBox);
		creditTexture.setLeftCenter(new Vector2f(-0.5f,0));
		topChildren.add(creditTexture);
		
		credits = new DynamicText("", new Vector2f(creditTexture.getRightCenter().x + 0.02f,textHeight));
		credits.useSettings(getSmallFont());
		credits.setScale(textScale);
		credits.getSettings().setColor(CREDIT_BLUE);
		displayBox.addText(credits);
		
		GuiModel cubitTexture = new GuiModel(new Vector2f(), creditTexture.getSize(), BuildState.cubitTexture, displayBox);
		cubitTexture.setLeftCenter(new Vector2f(0.25f, 0));
		topChildren.add(cubitTexture);
		
		cubits = new DynamicText("", new Vector2f(cubitTexture.getRightCenter().x + 0.02f,textHeight));
		cubits.useSettings(getSmallFont());
		cubits.setScale(textScale);
		cubits.getSettings().setColor(CUBIT_GOLD);
		displayBox.addText(cubits);
		
		darken = new GuiModel(new Vector2f(), new Vector2f(1,1), new Vector4f(0.3f,0.3f,0.3f,0.3f), this);
		bottomChildren.add(darken);
		
		itemTips = new ArrayList<ItemInfo>();
		for(BlockType block : Blocks.blocks) {
			for(Item item : block.getItems()) {
				itemTips.add(item.getInfoGui());
			}
		}
		topChildren.addAll(itemTips);
	}

	@Override
	public void update() {
		super.update();
		credits.setText(Player.getCredits()+"");
		cubits.setText(Player.getCubits()+"");
		tokens.setText(Player.getTokens(tokenLevel)+"");
		if(GuiManager.getGuiState(MENU).active()){
			PostProcessing.blur();
			darken.enable();
		}else{
			PostProcessing.unblur();
			darken.disable();
		}for(ItemInfo tip : itemTips) {
			tip.update();
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)){
			//open settings gui
		}
		MenuGui menu = (MenuGui) GuiManager.getGuiState(MENU);
		if(Keyboard.isKeyDown(Keyboard.KEY_C)){
			menu.switchState(BuildGui.SHOP);
		}else if(Keyboard.isKeyDown(Keyboard.KEY_Q)){
			menu.switchState(BuildGui.INVENTORY);
		}else if(Keyboard.isKeyDown(Keyboard.KEY_E)){
			menu.switchState(BuildGui.BATTLE);
		}else if(Keyboard.isKeyDown(Keyboard.KEY_T)){
			menu.switchState(BuildGui.TECH_TREE);
		}else if(Keyboard.isKeyDown(Keyboard.KEY_G)){
			menu.switchState(BuildGui.GARAGE);
		}
	}
	
	public static FontSettings getSmallFont(){
		return new FontSettings(defaultSmallFont);
	}
	
	public void close() {
		states.get(MENU).deactivate();
	}
}
