package game.states.buildState.gui;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.font.Text;
import renderEngine.guis.GuiManager;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiState;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;
import renderEngine.utils.MouseManager;

public class BuildGuiState extends GuiState{
	
	private GuiModel topStrip;
	private GuiModel stripBorder;
	private GuiModel menu;
	
	public BuildGuiState(){
		new GuiModel(new Vector2f(0.0f,0.0f), new Vector2f(0.1f , 0.1f * DisplayManager.getAspectRatio()), Loader.loadTexture("crosshair.png"), this);
		
		Vector4f stripColor = new Vector4f(BuildGui.GREY);
		stripColor.w = 0.75f;
		topStrip = new GuiModel(new Vector2f(), new Vector2f(1, 0.03f), stripColor, this);
		topStrip.setUpperCenter(new Vector2f(0, 1));
		
		stripBorder = new GuiModel(new Vector2f(), new Vector2f(1, 0.005f), new Vector4f(1,1,1,1), this);
		stripBorder.setUpperCenter(new Vector2f(0, 1 - topStrip.getSize().y*2));
		
		menu = new GuiModel(new Vector2f(), new Vector2f(0.05f, 0.02f), Loader.loadTexture("menuButton.png"), this);
		menu.setTopLeft(new Vector2f(-1, 1 - (topStrip.getSize().y + stripBorder.getSize().y) * 2));
		
		Text menuText = new Text("Menu[Tab]", new Vector2f(-0.15f, 0.2f));
		menuText.center();
		menuText.setScale(3.9f);
		menuText.useSettings(BuildGui.getSmallFont());
		menu.addText(menuText);
	}
	
	@Override
	public void update(){
		super.update();
		if(Keyboard.isKeyDown(Keyboard.KEY_TAB))
			GuiManager.getGuiState(BuildGui.MENU).activate();
	}
	
	@Override
	public void activate(){
		super.activate();
		MouseManager.hideMouse();
	}
}
