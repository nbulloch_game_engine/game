package game.states.buildState.gui;

import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.blocks.Blocks;
import game.entities.blocks.Item;
import game.states.buildState.BlockManager;
import renderEngine.guis.GridDisplay;
import renderEngine.guis.GuiState;
import renderEngine.guis.ScrollBar;

public class InventoryGui extends GuiState{

	private static ArrayList<Item> add = new ArrayList<Item>();
	private static ArrayList<Item> remove = new ArrayList<Item>();
	private static Item selected = Blocks.CUBE.getItem(0);
	
	private GridDisplay[] inventory;
	private GridDisplay active;
	
	public InventoryGui(){
		super(new Vector2f(), new Vector2f(1, 1-(1-MenuGui.minY)/2));
		setLowerCenter(new Vector2f(0, -1));
		
		float pad = 0.01f;
		Vector2f barSize = new Vector2f(0.005f, TabsGui.panel.getSize().y);
		Vector2f barPos = new Vector2f(0.99f, TabsGui.panel.getCenter().y);
		inventory = new GridDisplay[TabsGui.NUM_TABS];
		for(int i = 0; i < TabsGui.NUM_TABS; i++) {
			ScrollBar scroll = new ScrollBar(barPos, barSize, 0, this);
			scroll.setTrackColor(BuildGui.LIGHT_GREY);
			scroll.setInputColor(BuildGui.ORANGE);
			inventory[i] = new GridDisplay(new Vector2f(), new Vector2f(1-(TabsGui.panel.getSize().x+pad)-barSize.x, barSize.y), 13, scroll, 1.25f, this);
			inventory[i].setLeftCenter(new Vector2f(TabsGui.panel.getRightCenter().x+pad, barPos.y));
			inventory[i].setBackground(new Vector4f(0,0,0,0));
		}for(GridDisplay page : inventory) {
			page.disable();
		}
		active = inventory[0];
		active.enable();
		update();
	}
	
	@Override
	public void update() {
		super.update();
		for(Item item : add) {
			new InventoryCell(item, inventory[item.getTab()]);
		}add.clear();
		for(Item item : remove) {
			inventory[item.getTab()].removeCell(item.getTabIndex());
		}remove.clear();
		if(TabsGui.updated()) {
			active.disable();
			active = inventory[TabsGui.getActiveTab()];
			active.enable();	
		}
		active.update();
	}

	public static void addItems(HashMap<Item, Integer> items) {
		for(Item item : items.keySet()) {
			if(item.getNumOwned()==0)
				add.add(item);
			item.deltaOwned(items.get(item));
			if(item.getNumOwned()==0)
				remove.add(item);
		}
	}
	
	public static Item getSelected(){
		return selected;
	}
	
	public static boolean useBlock() {
		if(selected.getNumOwned()!=0) {
			selected.deltaOwned(-1);
			if(selected.getNumOwned()==0) {
				remove.add(selected);	
			}
			return true;
		}
		return false;
	}
	
	@Override
	public void activate() {
		super.activate();
		MenuGui.guiTip.setText("SELECT A CUBE");
		if(inventory!=null) {
			active.disable();
			active = inventory[TabsGui.getActiveTab()];
			active.enable();
		}
	}

	public static void select(Item item) {
		selected = item;
		BlockManager.setTexturedModel(item.getModel());
	}
}
