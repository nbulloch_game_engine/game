package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiState;

public class BattleGui extends GuiState {

	@SuppressWarnings("unused")
	private GuiModel strip;
	
	public BattleGui(){
		super(new Vector2f(), new Vector2f(1, 1-(1-MenuGui.minY)/2));
		setLowerCenter(new Vector2f(0, -1));
		
		strip = new GuiModel(new Vector2f(), new Vector2f(1, 0.45f), new Vector4f(0.2f, 0.2f, 0.2f, 0.6f), this);
	}
	
	@Override
	public void update(){}
}
