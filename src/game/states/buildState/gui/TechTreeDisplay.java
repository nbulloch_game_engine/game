package game.states.buildState.gui;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector4f;

import game.entities.Player;
import game.entities.blocks.techTree.TechTree;
import game.entities.blocks.techTree.Unlockable;
import renderEngine.font.Text;
import renderEngine.guis.GuiComponent;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;
import renderEngine.utils.MouseManager;

public class TechTreeDisplay extends GuiComponent {
	
	private static final boolean UNLOCK_ALL = Player.DEV_MODE;
	
	protected static final float HEX_HEIGHT = 0.25f;
	private static final float MAX_SCALE = 1.6f;
	private static final float MIN_SCALE = 0.7f;
	private static final Vector2f MAX = new Vector2f(1f, 0f);
	private static final Vector2f MIN = new Vector2f(-0.8f, -1f);
	private static final int startTexture = Loader.loadTexture("hexStart.png");
	
	private TechTreeCell[][] grid;
	private float scale = 1;
	
	private Vector2f displaySize;
	
	public TechTreeDisplay(int x, int y, GuiComponent parent){
		super(new Vector2f(), new Vector2f(1, 1), parent);
		float colOffset = 1.5f * HEX_HEIGHT / (float)Math.sqrt(3) / DisplayManager.getAspectRatio();
		displaySize = new Vector2f((x - 1) * colOffset / 2, (y - 1) * HEX_HEIGHT / 2);
		grid = new TechTreeCell[x][y];
		for(int i = 0; i < x; i++){
			boolean odd = i % 2 == 0;
			int rows = odd ? y : y - 1;
			float xPos = i * colOffset;
			for(int j = 0; j < rows; j++){
				Vector2f position = new Vector2f(displaySize);
				position.x -= xPos;
				position.y -= j * HEX_HEIGHT;
				position.y -= HEX_HEIGHT / 2;
				if(odd)
					position.y += HEX_HEIGHT/2;
				TechTreeCell cell = new TechTreeCell(position, this);
				grid[i][j] = cell;
			}
		}
		displaySize.x+=colOffset / 3 * 2;
		displaySize.y+=HEX_HEIGHT / 2;
		populateTree(x, y);
		
		MAX.y = MenuGui.minY;
	}
	
	private void populateTree(int sizeX, int sizeY){
		Unlockable[] leaves = TechTree.start.getLeaves();
		int startX = sizeX / 2;
		int startY = sizeY / 2;
		grid[startX][startY].setOverlayTexture(startTexture);
		
		Text start = new Text("START", new Vector2f());
		start.center();
		start.useSettings(BuildGui.getSmallFont());
		start.getSettings().setColor(new Vector4f(0,0,0,1));
		grid[startX][startY].addText(start);
		
		addLeaf(leaves[0], startX, startY-1);
		addLeaf(leaves[3], startX, startY+1);
		if(startX%2==0)
			startY--;
		addLeaf(leaves[1], startX-1, startY);
		addLeaf(leaves[2], startX-1, startY+1);
		addLeaf(leaves[4], startX+1, startY+1);
		addLeaf(leaves[5], startX+1, startY);
	}
	
	private void addLeaf(Unlockable leaf, int x, int y){
		if(leaf != null){
			grid[x][y].setUnlockable(leaf);
			leaf.setCoords(new Vector2f(x, y));
			Unlockable[] leaves = leaf.getLeaves();
			addLeaf(leaves[0], x, y-1);
			addLeaf(leaves[3], x, y+1);
			if(x%2==0)
				y--;
			addLeaf(leaves[1], x-1, y);
			addLeaf(leaves[2], x-1, y+1);
			addLeaf(leaves[4], x+1, y+1);
			addLeaf(leaves[5], x+1, y);
			if(UNLOCK_ALL){
				leaf.purchase();
			}
		}
	}
	
	public void reset(){
		scale = 1;
		setSize(new Vector2f(scale, scale));
		setPosition(new Vector2f());
	}
	
	public void update(){
		float dWheel = (float)Mouse.getDWheel() / 1000;
		if(dWheel!=0){
			scale += dWheel;
			if(scale>MAX_SCALE){
				scale = MAX_SCALE;
			}else if(scale<MIN_SCALE){
				scale = MIN_SCALE;
			}
			setSize(new Vector2f(scale, scale));
		}
		if(Mouse.isButtonDown(0)){
			changePosition(new Vector2f((float)MouseManager.getDX() / (float)Display.getWidth() * 2, (float)MouseManager.getDY() / (float)Display.getHeight() * 2));
		}
		if(Mouse.isButtonDown(0) || dWheel!=0){
			Vector2f pos = getCenter();
			if(pos.y + displaySize.y * scale<MAX.y){
				pos.y = MAX.y - displaySize.y * scale;
			}else if(pos.y - displaySize.y * scale>MIN.y){
				pos.y = MIN.y + displaySize.y * scale;
			}
			if(pos.x + displaySize.x * scale<MAX.x){
				pos.x = MAX.x - displaySize.x * scale;
			}else if(pos.x - displaySize.x * scale>MIN.x){
				pos.x = MIN.x + displaySize.x * scale;
			}
			setPosition(pos);
		}
		updateCells();
	}
	
	public void updateCells(){
		for(TechTreeCell[] row : grid){
			for(TechTreeCell cell : row){
				if(cell!=null)
					cell.update();
			}
		}
	}

	public void unlockAdjacent(Unlockable unlockable) {
		int x = (int) unlockable.getCoords().x;
		int y = (int) unlockable.getCoords().y;
		unlockAtCoords(x, y-1);
		unlockAtCoords(x, y+1);
		if(x%2==0)
			y--;
		unlockAtCoords(x-1, y);
		unlockAtCoords(x-1, y+1);
		unlockAtCoords(x+1, y+1);
		unlockAtCoords(x+1, y);
	}
	
	public void unlockAtCoords(int x, int y){
		if(x>=0&&x<grid.length)
			if(y>=0&&y<grid[0].length)
				grid[x][y].unlock();
	}
}
