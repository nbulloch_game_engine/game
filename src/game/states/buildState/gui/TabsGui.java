package game.states.buildState.gui;

import org.lwjgl.util.vector.Vector2f;
import renderEngine.font.FontSettings;
import renderEngine.font.Text;
import renderEngine.guis.Button;
import renderEngine.guis.GuiModel;
import renderEngine.guis.GuiPlacer;
import renderEngine.guis.GuiState;
import renderEngine.renderEngine.Loader;

public class TabsGui extends GuiState{

	public static int NUM_TABS = 0;
	public static final int BLOCK_TAB = NUM_TABS++;
	public static final int FUNCTIONAL_TAB = NUM_TABS++;//weapons & plates
	public static final int MOVEMENT_TAB = NUM_TABS++;
	//public static final int SPECIAL_TAB = NUM_TABS++;
	
	private static final int hexCap = Loader.loadTexture("shopHexCap.png");
	private static final int hoverTexture = Loader.loadTexture("sideButtonHover.png");
	public static final Integer[][] ICONS = loadTextures();
	
	public static GuiModel panel;
	
	private static int activeTab = 0;
	private static boolean updated = false;
	
	private Button[] buttons;
	private GuiModel[] icons;
	
	public TabsGui() {
		super(new Vector2f(), new Vector2f(1, 1-(1-MenuGui.minY)/2));
		setLowerCenter(new Vector2f(0, -1));
		
		GuiPlacer.init(new Vector2f(), new Vector2f(0.125f,1),  this);
		GuiPlacer.setYRatio(0.0625f);
		GuiPlacer.setLowerLeft(new Vector2f(-1, -1));
		new GuiModel(hexCap);
		GuiPlacer.setLowerRight(new Vector2f(1, -1));
		new GuiModel(hexCap).mirrorX();
		float center = GuiPlacer.getSize().y;
		GuiPlacer.init(new Vector2f(), new Vector2f(0.125f*0.9375f, 1-(GuiPlacer.getSize().y+0.005f)*2), this);
		GuiPlacer.setLeftCenter(new Vector2f(-1, center));
		panel = new GuiModel(BuildGui.GREY);
		
		FontSettings settings = new FontSettings();
		settings.setColor(BuildGui.ORANGE);
		settings.setRenderWidth(0.5f);
		settings.setFadeWidth(0.3f);
		String[] texts = {"CHASSIS", "MOVEMENT", "FUNCTIONAL"};
		
		buttons = new Button[NUM_TABS];
		GuiPlacer.init(new Vector2f(), new Vector2f(0.97f,1), panel);
		GuiPlacer.setYRatio(0.25f);
		float size = GuiPlacer.getSize().y+0.001f;
		float height = size*NUM_TABS;
		for(int i = 0; i < buttons.length; i++) {
			GuiPlacer.setUpperCenter(new Vector2f(-0.015f, height-size*i*2));
			buttons[i] = new Button();
			buttons[i].mirrorX();
			buttons[i].setTextures(-1, hoverTexture);
			buttons[i].setTextHoverColor(BuildGui.GREY);
			
			Text text = new Text(texts[i], new Vector2f(-0.4f, -0.17f));
			text.setScale(2.5f);
			text.useSettings(new FontSettings(settings));
			buttons[i].addText(text);
		}
		buttons[activeTab].setSelected(true);
		
		icons = new GuiModel[NUM_TABS];
		GuiPlacer.init(new Vector2f(-0.7f, 0.1f), new Vector2f(1, -0.8f), buttons[0]);
		GuiPlacer.setXRatio(1);
		for(int i = 0; i < icons.length; i++) {
			GuiPlacer.init(GuiPlacer.getPos(), GuiPlacer.getSize(), buttons[i]);
			icons[i] = new GuiModel(ICONS[0][i]);
			icons[i].mirrorY();
			icons[i].mirrorX();
		}icons[activeTab].switchTexture(ICONS[1][activeTab]);
		selectTab(BLOCK_TAB);
		update();
	}
	
	@Override
	public void update() {
		if(updated)
			updated = false;
		for(int i = 0; i < buttons.length; i++) {
			buttons[i].update();
			if(buttons[i].isActivated()) {
				selectTab(i);
			}
		}
	}
	
	private void selectTab(int tab) {
		if(activeTab!=tab) {
			buttons[activeTab].setSelected(false);
			buttons[tab].setSelected(true);
			icons[activeTab].switchTexture(ICONS[0][activeTab]);
			activeTab = tab;
			updated = true;
			icons[tab].switchTexture(ICONS[1][tab]);
		}
	}
	
	public static int getActiveTab() {
		return activeTab;
	}
	
	public static boolean updated() {
		return updated;
	}
	
	public static Integer[][] loadTextures(){
		Integer[][] i = new Integer[2][NUM_TABS];
		String[] file = {"cubeIcon", "movementIcon", "functionalIcon"};
		for(int j = 0; j < file.length; j++) {
			i[1][j] = Loader.loadTexture(file[j]+".png");
			i[0][j] = Loader.loadTexture(file[j]+"Orange.png");
		}
		return i;
	}
}
