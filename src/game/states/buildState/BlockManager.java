package game.states.buildState;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;

import game.entities.Bot;
import game.entities.Player;
import game.entities.blocks.Blocks;
import game.entities.blocks.Item;
import game.states.buildState.gui.InventoryGui;
import physicsEngine.collisionDetection.CollisionDetection;
import physicsEngine.collisionDetection.primitives.AABB;
import physicsEngine.collisionDetection.primitives.Plane;
import physicsEngine.collisionDetection.primitives.Ray;
import physicsEngine.collisionDetection.primitives.Triangle;
import renderEngine.entities.Camera;
import renderEngine.entities.Entity;
import renderEngine.models.TexturedModel;
import renderEngine.utils.Quaternion;
import renderEngine.utils.Vector3i;

public class BlockManager {

	private static final Float HALF_PI = (float)Math.PI/2f;
	
	private static Bot bot;
	private static Entity selected;
	
	private static int floorRadius = BuildScene.gridSize/2;
	private static int floorSize = BuildScene.gridSize;
	private static final float e = 0.000005f;
	public static final AABB GRID_BOUNDS = new AABB(new Vector3f(floorRadius-1, floorRadius-e/2, floorRadius-1), new Vector3f(floorRadius-e, floorRadius-e/2, floorRadius-e));
	private static final float cam_pad = 2;
	public static final AABB CAM_BOUNDS = new AABB(new Vector3f(floorRadius, floorRadius+cam_pad/2, floorRadius), new Vector3f(floorRadius+cam_pad, floorRadius+cam_pad/2, floorRadius+cam_pad));
	private static final Vector3i terrainNormal = new Vector3i(0,1,0);
	private static Plane floor = new Plane(new Vector3f(0,1,0), new Vector3f());
	private static boolean inbounds;
	private static Entity pickedBlock;
	private static Vector3i pickedNormal;
	private static Vector3f pickedIndices;//the integer coordinates of the selected space
	private static int wheel;
	private static Quaternion rotation;
	private static final HashSet<Vector3i> axisKey = assignKeys();
	private static final Quaternion[] baseRotations = calculateBaseRotations();
	private static final Quaternion[] rotations = calculateRotations();
	
	//repeat placement
	private static long delta, lastClick, placeSpeed = 250, speedPlaceActivate = 500, fastPlaceSpeed = 90, lastFrame;
	private static boolean speedPlaceActive = false;
	private static int mouseEvent = -1;
	
	public static void init() {
		//Load bots
		bot = new Bot();
		Player.addBot(bot);
		selected = new Entity(Blocks.CUBE.getItem(0).getModel(), new Vector3f(), new Vector3f(), new Vector3f(0.5f,0.5f,0.5f), BuildScene.cubeScale);
		selected.setScale(BuildScene.cubeScale);
		selected.setTranslucency(0.5f);
		
		rotation = new Quaternion(new Vector3f(0,1,0), 0);
	}
	
	public static void update(Camera camera) {
		recordMouse();
		Vector3f v1 = Vector3f.add(camera.getPosition(), camera.getDirection(), null);
		Ray picker = new Ray(camera.getPosition(), v1);
		Vector3f terrain = CollisionDetection.rayPlaneI(floor, picker);
		float nearest = nearestCollision(getLooseCollisions(picker), picker);
		inbounds = (terrain!=null //on the floor
				&&terrain.x>=0&&terrain.x<=floorSize&&terrain.z>=0&&terrain.z<floorSize)//and within the floor grid
				||nearest!=Float.MAX_VALUE;//or intersecting with a block
		if(inbounds) {
			if(terrain!=null && nearest>Vector3f.sub(camera.getPosition(), terrain, null).lengthSquared()) {
				pickedNormal = terrainNormal;
				pickedIndices = terrain;
				pickedIndices.x-=pickedIndices.x%1;
				pickedIndices.y = 0;
				pickedIndices.z-=pickedIndices.z%1;
			}else if(nearest!=Float.MAX_VALUE){
				pickedIndices = new Vector3f(pickedIndices);
				if(canPlace(1)) {
					bot.removeBlock(pickedBlock);
					inbounds = false;
					return;
				}if(Keyboard.isKeyDown(Keyboard.KEY_M)) {
					Item item = bot.getItem(pickedBlock);
					InventoryGui.select(item);
					selected.setTexturedModel(item.getModel());
				}if(!pickedNormal.created()) {
					inbounds = false;//stop rendering selected
					return;
				}
				Vector3i.add(pickedIndices, pickedNormal, pickedIndices);
				
			}else{
				inbounds = false;
				return;
			}
			updateRotation();
			selected.setRotation(rotation);
			selected.setPosition(pickedIndices);
			
			if(validSpace(pickedIndices)) {
				if(canPlace(0)) {
					bot.addBlock(InventoryGui.getSelected(), pickedIndices, new Quaternion(rotation));
				}
			}else{
				inbounds = false;
			}
			lastFrame = System.currentTimeMillis();
		}
	}
	
	private static boolean validSpace(Vector3f block) {
		boolean blockExists = false;
		for(Entity e : bot.getEntities()) {
			if(e.getPosition().equals(block)) {
				blockExists = true;
				break;
			}
		}
		if(!GRID_BOUNDS.contains(block)||blockExists) {
			inbounds = false;
			return false;
		}
		return true;
	}
	
	private static void recordMouse() {
		mouseEvent = -1;
		while(Mouse.next()) {
			int button = Mouse.getEventButton();
			if(button!=-1) {
				mouseEvent = button;
				if(button==1) {
					return;
				}
			}
		}
	}
	
	private static boolean canPlace(int mouseButton) {
		if(Mouse.isButtonDown(mouseButton)) {
			if(mouseEvent==mouseButton) {
				delta = 0;
				lastClick = System.currentTimeMillis();
				speedPlaceActive = false;
				return true;
			}
			
			//rapid or repeated placement
			long currentSpeed = placeSpeed;
			if(speedPlaceActive) {
				currentSpeed = fastPlaceSpeed;
			}else if(System.currentTimeMillis()-lastClick>speedPlaceActivate){
				speedPlaceActive = true;
			}
			delta += System.currentTimeMillis()-lastFrame;
			if(currentSpeed<=delta) {
				delta -= currentSpeed;
				return true;
			}
		}
		return false;
	}
	
	private static void updateRotation() {
		wheel += Mouse.getDWheel()/120;
		if(wheel<0) {
			wheel=4+wheel;
		}wheel%=4;
		int axisIndex = getAxis(pickedNormal);
		if(axisIndex!=-1) {
			Quaternion.multiply(baseRotations[getAxis(pickedNormal)], rotations[wheel], rotation);
		}else {
			rotation.setValues(rotations[wheel]);
		}
	}
	
	private static float nearestCollision(ArrayList<Entity> looseCollisions, Ray picker){
		float distance = Float.MAX_VALUE;
		for(Entity entity : looseCollisions) {
			for(Triangle t : entity.getModel().getTriangles()) {
				Vector3f tIntersection = CollisionDetection.getRayTriangleI(t, picker);
				if(tIntersection!=null) {
					float tDistance = Vector3f.sub(tIntersection, picker.v0, null).lengthSquared();
					if(tDistance < distance) {
						distance = tDistance;
						pickedNormal = new Vector3i(t.p.n, e);
						pickedIndices = entity.getPosition();
						pickedBlock = entity;
					}
				}
			}
		}
		return distance;
	}
	
	private static ArrayList<Entity> getLooseCollisions(Ray picker){
		ArrayList<Entity> collisions = new ArrayList<Entity>();
		for(Entity block : bot.getEntities()) {
			if(CollisionDetection.aabbRayI(block.getModel().getAABB(), picker)!=null) {
				collisions.add(block);
			}
		}
		return collisions;
	}
	
	public static ArrayList<Entity> getEnities() {
		ArrayList<Entity> out = new ArrayList<Entity>(bot.getEntities());
//		if(inbounds)
//			out.add(selected);
		return out;
	}
	
	public static void setTexturedModel(TexturedModel model) {
		selected.setTexturedModel(model);
	}
	
	private static int getAxis(Vector3i axis) {
		Iterator<Vector3i> key = axisKey.iterator();
		int i = 0;
		while(key.hasNext()) {
			if(key.next().equals(axis))
				return i;
			i++;
		}return -1;
	}
	
	private static HashSet<Vector3i> assignKeys(){
		HashSet<Vector3i> keys = new HashSet<Vector3i>();
		Vector3i[] axisKey = {
				new Vector3i(1,0,0),
				new Vector3i(-1,0,0),
				new Vector3i(0,-1,0),
				new Vector3i(0,0,1),
				new Vector3i(0,0,-1)
				};
		for(int i = 0; i < axisKey.length; i++) {
			keys.add(axisKey[i]);
		}
		return keys;
	}
	
	private static Quaternion[] calculateBaseRotations() {
		Quaternion[] rotations = new Quaternion[axisKey.size()];
		Iterator<Vector3i> axis = axisKey.iterator();
		int i = 0;
		while(axis.hasNext()) {
			Vector3i integer = new Vector3i(axis.next());
			Vector3f cross = new Vector3f(integer.x, integer.y, integer.z);
			if(cross.y!=0) {
				cross.y = 0;
				cross.x = 1;
				rotations[i] = new Quaternion(cross, HALF_PI*2);
			}else {
				cross.set(cross.z, 0, -cross.x);
				rotations[i] = new Quaternion(cross, HALF_PI);
			}
			i++;
		}
		return rotations;
	}
	
	private static Quaternion[] calculateRotations() {
		Quaternion[] rotations = new Quaternion[4];
		for(int i = 0; i < rotations.length; i++) {
			rotations[i] = new Quaternion(floor.n, HALF_PI*i);
		}
		return rotations;
	}
	
}
