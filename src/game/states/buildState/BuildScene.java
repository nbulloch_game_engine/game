package game.states.buildState;

import java.util.ArrayList;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import renderEngine.entities.Camera;
import renderEngine.entities.DirectionalLight;
import renderEngine.entities.Entity;
import renderEngine.entities.PointLight;
import renderEngine.entities.Scene;
import renderEngine.models.TexturedModel;
import renderEngine.renderEngine.Loader;
import renderEngine.terrain.Terrain;
import renderEngine.textures.TerrainTexture;

public class BuildScene extends Scene {
	
	private static final Vector3f skyColor = new Vector3f(0.5f, 0.5f, 0.5f);
	private static final Vector4f clipPlane = new Vector4f();
	private static final int MAX_SHADOW_MAP = 8192;
	private static final int MIN_SHADOW_MAP = 2048;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 95;
	private static final int FOV = 57;
	
	public static final int gridSize = 21;
	public static final float cubeScale = 0.5f;
	
	private static float cubeHeight;
	private static float cubeSize;
	
	public BuildScene(){
		staticNormalMapEntities = new ArrayList<Entity>();
		staticEntities = new ArrayList<Entity>();
		terrains = new ArrayList<Terrain>();
		lights = new ArrayList<PointLight>();
		DLights = new ArrayList<DirectionalLight>();
		Vector3f lightColor = new Vector3f(0.8f, 0.8f, 0.8f);
		sun = new DirectionalLight(new Vector3f(0.2f, -0.8f, 0.1f), lightColor);
		DLights.add(sun);
		TerrainTexture gridTexture = new TerrainTexture(-1);
		gridTexture.setTransparency(true);
		cubeHeight = 2;
		cubeSize = cubeHeight*cubeScale;
		float buildBaseScale = cubeScale*gridSize;
		Entity buildBase = new Entity(new TexturedModel("buildBase", "buildBase.png", false), new Vector3f(buildBaseScale, 0.1f, buildBaseScale), new Vector3f(), new Vector3f(), new Vector3f(buildBaseScale,buildBaseScale,buildBaseScale));
		TexturedModel buildBaseGridTexture = new TexturedModel("buildBaseGrid", Loader.loadTextureUnclamped("buildBaseGrid.png"), false);
		buildBaseGridTexture.setTiles(gridSize);
		buildBaseGridTexture.setTranslucent(true);
		buildBaseGridTexture.setCastsShadows(false);
		buildBaseGridTexture.getTexture().setTransparency(true);
		buildBaseGridTexture.getTexture().setLightMap(Loader.loadTextureUnclamped("buildBaseGridLightMap.png"));
		buildBaseGridTexture.getTexture().setGlowMap(Loader.loadTexture("glowMap.png"));
		buildBaseGridTexture.getTexture().setBloomWidth(0.1f);
		buildBaseGridTexture.getTexture().setGlowMapFactor(0.1f);
		buildBaseGridTexture.getTexture().setGlowSpeed(0.01f);
		Entity buildBaseGrid = new Entity(buildBaseGridTexture, new Vector3f(buildBaseScale, 0, buildBaseScale), new Vector3f(), new Vector3f(), new Vector3f(buildBaseScale,buildBaseScale,buildBaseScale));
		staticEntities.add(buildBase);
		staticEntities.add(buildBaseGrid);
		
		int terrainSize = (int) (cubeSize*gridSize);
		terrain = new Terrain(0f, 0f, gridTexture, 16, terrainSize);
		terrain.setTiles(gridSize);
		terrains.add(terrain);
		camera = new Camera(new Vector3f(10,1,10), Camera.NLERP);
		camera.setSpeed(20*cubeScale);
		csmShadows = false;
	}
	
	@Override
	public float getNearPlane() {
		return NEAR_PLANE;
	}

	@Override
	public float getFarPlane() {
		return FAR_PLANE;
	}

	@Override
	public int getNumCascades() {
		return 5;
	}

	@Override
	public Vector3f getSkyColor() {
		return skyColor;
	}

	@Override
	public Vector4f getClipPlane() {
		return clipPlane;
	}

	@Override
	public float getFogDensity() {
		return 0.0f;
	}

	@Override
	public float getFogGradient() {
		return 1.0f;
	}

	@Override
	public float getFogginess() {
		return 0.0f;
	}

	@Override
	public float getFOV() {
		return FOV;
	}
	
	@Override
	public int getMaxShadowSize() {
		return MAX_SHADOW_MAP;
	}

	@Override
	public int getMinShadowSize() {
		return MIN_SHADOW_MAP;
	}
}
