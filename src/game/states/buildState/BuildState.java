package game.states.buildState;

import game.entities.blocks.Blocks;
import game.states.GameState;
import game.states.buildState.gui.BuildGui;
import physicsEngine.physicsEngine.PhysicsManager;

import java.util.ArrayList;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;

import renderEngine.entities.Entity;
import renderEngine.font.TextMaster;
import renderEngine.fontRenderer.FontRenderer;
import renderEngine.guiRenderer.GuiRenderer;
import renderEngine.guis.Button;
import renderEngine.guis.GuiManager;
import renderEngine.postProcessing.Fbo;
import renderEngine.postProcessing.PostProcessing;
import renderEngine.renderEngine.DisplayManager;
import renderEngine.renderEngine.Loader;
import renderEngine.renderEngine.MasterRenderer;
import renderEngine.terrain.Terrain;
import renderEngine.utils.MouseManager;
import renderEngine.utils.MousePicker;
import renderEngine.utils.ScreenShot;

public class BuildState implements GameState{
	
	public static int creditTexture = Loader.loadTexture("creditBig.png");
	public static int cubitTexture = Loader.loadTexture("cubitBig.png");
	
	private MasterRenderer renderer;
	private MousePicker picker;
	private ArrayList<Entity> entities;
	private ArrayList<Entity> normalMapEntities;
	private Fbo MSFbo;
	private Fbo outputFbo;
	private Fbo outputFboBloom;
	
	private BuildScene scene;
	
	private BuildGui buildGui;
	
	private Entity physics;
	
	public BuildState(){
		entities = new ArrayList<Entity>();
		normalMapEntities = new ArrayList<Entity>();
		scene = new BuildScene();
		renderer = new MasterRenderer(scene);
		
		picker = new MousePicker(scene.getCamera(), renderer.getProjectionMatrix(), scene.getTerrains().get(0));//no terrains?
		
		buildGui = new BuildGui();
		
		MSFbo = new Fbo(Display.getWidth(), Display.getHeight(), (byte)2);
		outputFbo = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		outputFboBloom = new Fbo(Display.getWidth(), Display.getHeight(), Fbo.DEPTH_TEXTURE);
		
		
		physics = new Entity(Blocks.CUBE.getItem(7).getModel(), new Vector3f(), new Vector3f(), new Vector3f(), 0.5f);
	}
	
	public void update(){
		MouseManager.updateCursor();
		Button.updatePressedButton();
		if(Keyboard.isKeyDown(Keyboard.KEY_F11))
			ScreenShot.render();
		if(Keyboard.isKeyDown(Keyboard.KEY_Y)){
			Vector3f target = new Vector3f(0,1,0);
			float time = 5;
			scene.getCamera().lookAtSmooth(target, time);
		}
		
		BlockManager.CAM_BOUNDS.nearestPoint(scene.getCamera().getPosition(), scene.getCamera().getPosition());
		
		buildGui.update();
		picker.update();
		if(!GuiManager.getGuiState(BuildGui.MENU).active()) {
			scene.getCamera().update();
			BlockManager.update(scene.getCamera());
		}
		
		entities = new ArrayList<Entity>();
		entities.addAll(scene.getStaticEntities());
		entities.add(physics);
		normalMapEntities = new ArrayList<Entity>();
		normalMapEntities.addAll(scene.getStaticNormalMapEntities()); 
		normalMapEntities.addAll(BlockManager.getEnities());
		
		renderer.updateCsm();
		render();
	}
	
	public void render(){
		renderer.renderShadowMap(entities, normalMapEntities, scene.getSun());
		MSFbo.bindFrameBuffer();
		renderer.renderScene(entities, normalMapEntities, new ArrayList<Terrain>());
		MSFbo.unbindFrameBuffer();
		MSFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT0, outputFbo);
		MSFbo.resolveToFbo(GL30.GL_COLOR_ATTACHMENT1, outputFboBloom);
		PostProcessing.doPostProcessing(outputFbo.getColorTexture(), outputFboBloom.getColorTexture());
		
		buildGui.render();
		TextMaster.render();
		DisplayManager.updateDisplay();
	}
	
	public void clean(){
		PostProcessing.clean();
		renderer.clean();
		GuiRenderer.clean();
		FontRenderer.clean();
		MSFbo.clean();
		outputFbo.clean();
		outputFboBloom.clean();
	}

	public void integrate(){
		//no physics
	}
}