package game.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.lwjgl.util.vector.Vector3f;

import game.entities.blocks.Item;
import game.states.buildState.BuildScene;
import renderEngine.entities.Entity;
import renderEngine.utils.Quaternion;

public class Bot implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private HashMap<Item, ArrayList<Entity>> blocks = new HashMap<Item, ArrayList<Entity>>();
	private ArrayList<Entity> entities = new ArrayList<Entity>();
	
	public void addBlock(Item block, Vector3f position, Quaternion rotation) {
		ArrayList<Entity> batch = blocks.get(block);
		Entity entity = new Entity(block.getModel(), position, rotation, new Vector3f(0.5f,0.5f,0.5f), BuildScene.cubeScale);
		if(batch == null) {
			batch = new ArrayList<Entity>();
			batch.add(entity);
			blocks.put(block, batch);
		}else {
			batch.add(entity);
		}
		entities.add(entity);
	}
	
	public void removeBlock(Item block, Vector3f position) {
		ArrayList<Entity> batch = blocks.get(block);
		if(batch != null) {
			for(int i = 0; i < batch.size(); i++) {
				Entity entity = batch.get(i);
				if(batch.get(i).getPosition().equals(position)) {
					batch.remove(i);
					entities.remove(entity);
					break;
				}
			}
		}
	}
	
	public void removeBlock(Entity block) {
		Iterator<Item> i = blocks.keySet().iterator();
		while(i.hasNext()) {
			ArrayList<Entity> list = blocks.get(i.next());
			for(Entity entity : list) {
				if(entity.equals(block)) {
					list.remove(entity);
					entities.remove(block);
					break;
				}
			}
		}
	}
	
	public Item getItem(Entity block) {
		Iterator<Item> i = blocks.keySet().iterator();
		while(i.hasNext()) {
			Item item = i.next();
			for(Entity entity : blocks.get(item)) {
				if(entity.equals(block)) {
					return item;
				}
			}
		}return null;
	}
	
	public ArrayList<Entity> getEntities(){
		return entities;
	}
}
