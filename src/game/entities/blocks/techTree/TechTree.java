package game.entities.blocks.techTree;

import game.entities.blocks.Blocks;

public class TechTree {

	public static Unlockable start;
	
	private static Unlockable[] cube = Blocks.CUBE.getUnlockables();
	private static Unlockable[] prism = Blocks.PRISM.getUnlockables();
	private static Unlockable[] inner = Blocks.INNER.getUnlockables();
	private static Unlockable[] tetra = Blocks.TETRA.getUnlockables();
	private static Unlockable[] core = Blocks.CORE.getUnlockables();
	
	public static void createTree(){
		start = new Unlockable();
		
		cube[0].purchase();
		prism[0].purchase();
		inner[0].purchase();
		tetra[0].purchase();
		core[0].purchase();
		unlock(core[1]);
		
		start.addBranch(core[1], 0);
		core[1].addBranch(cube[1], -1);
		cube[1].addBranch(prism[1], -1);
		prism[1].addBranch(inner[1], -1);
		inner[1].addBranch(tetra[1], -1);
	}
	
	public static void unlock(Unlockable leaf){
		leaf.unlock();
	}
}
