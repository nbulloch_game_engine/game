package game.entities.blocks.techTree;

import java.io.File;
import java.util.HashMap;

import renderEngine.renderEngine.Loader;

public class Icons {

	private static HashMap<String, Integer> icons;
	
	public static HashMap<String, Integer> loadIcons(){
		icons = new HashMap<String, Integer>();
		File iconFolder = new File("res/icons");
		for(String path : iconFolder.list()){
			icons.put(path.substring(0, path.length()-4), Loader.loadTexture("icons\\"+path));
		}
		return icons;
	}
	
	public static int get(String path){
		return icons.get(path);
	}
}
