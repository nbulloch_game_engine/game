package game.entities.blocks.techTree;

import game.entities.blocks.Item;
import game.states.buildState.gui.ShopGui;

public class UnlockableItem extends Unlockable{

	private Item item;
	
	public UnlockableItem(int tokenLevel, int tokenCost, int direction, Item item){
		super(item.getName(), tokenLevel, tokenCost, item.getIcon(), direction);
		this.item = item;
	}
	
	public UnlockableItem(int tokenLevel, int tokenCost, Item item){
		super(item.getName(), tokenLevel, tokenCost, item.getIcon());
		this.item = item;
	}
	
	public UnlockableItem(Item item){
		super();
		this.item = item;
	}
	
	public Item getItem() {
		return item;
	}
	
	@Override
	public void purchase() {
		purchased = true;
		if(item.getTab()!=-1)
			ShopGui.addItem(item);
	}

}
