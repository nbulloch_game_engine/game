package game.entities.blocks.techTree;

import org.lwjgl.util.vector.Vector2f;

public class Unlockable {
	
	private int direction;
	private Unlockable[] leaves;
	
	//leaf indices (direction) diagram
	//				__
	//			 __/ 0\__
	//			/ 5\__/ 1\
	//			\__/  \__/
	//			/ 4\__/ 2\
	//			\__/ 3\__/
	//			   \__/
	
	private int tokenLevel;
	private int tokenCost;
	private boolean unlocked = false;
	protected boolean purchased = false;
	private int icon;
	private String name;
	
	private Vector2f coords = new Vector2f();
	
	public Unlockable(String name, int tokenLevel, int tokenCost, int icon, int direction){
		this(name, tokenLevel, tokenCost, icon);
		this.direction = direction;
	}
	
	public Unlockable(String name, int tokenLevel, int tokenCost, int icon){
		leaves = new Unlockable[6];
		this.name = name;
		this.tokenLevel = tokenLevel;
		this.tokenCost = tokenCost;
		this.icon = icon;
	}
	
	public Unlockable(){
		leaves = new Unlockable[6];
	}
	
	public Unlockable[] getLeaves(){
		return leaves;
	}
	
	public Unlockable addBranch(Unlockable leaf, int deltaDirection){
		int direction = deltaDirection + this.direction;
		if(direction<0){
			direction = 6 - (Math.abs(direction) % 6);
		}else{
			direction %= 6;
		}
		leaf.setDirection(this.direction);
		leaves[direction] = leaf;
		return this;
	}
	
	public Unlockable setLeaf(Unlockable leaf){
		leaf.setDirection(direction);
		leaves[direction] = leaf;
		return this;
	}
	
	public Unlockable setParent(Unlockable parent){
		parent.setLeaf(this);
		return this;
	}
	
	public Unlockable setDirection(int direction){
		this.direction = direction;
		return this;
	}
	
	public int getTokenCost(){
		return tokenCost;
	}
	
	public int getTokenLevel(){
		return tokenLevel;
	}
	
	protected void unlock(){
		unlocked = true;
	}
	
	public void purchase(){
		this.purchased = true;
	}
	
	public boolean unlocked(){
		return unlocked;
	}
	
	public boolean purchased(){
		return purchased;
	}
	
	public int getIcon(){
		return icon;
	}
	
	public String getName(){
		return name;
	}
	
	public void setCoords(Vector2f coords){
		this.coords = coords;
	}
	
	public Vector2f getCoords(){
		return coords;
	}
}
