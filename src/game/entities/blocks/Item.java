package game.entities.blocks;

import game.states.buildState.gui.ItemInfo;
import renderEngine.models.TexturedModel;

public class Item {
	
	private static final float cubitRatio = 0.3f;
	
	protected String stats;
	private ItemInfo info;
	private TexturedModel model;
	
	private String name;
    private float mass;
	private int armor;
	private int pFLOPS;
	private int ranking;
	private int cost, cubitCost;
	private int tabIndex;
	private int tab;
	private int icon;
	private int numOwned = 0;
	private int tier;

	public Item(String name, int cost, int mass, int pFLOPS, int ranking, int armor, int tab, int icon, int blockId, int tier, String description, TexturedModel model) {
		this.name = name;
		this.cost = cost;
		this.cubitCost = (int)(cost * cubitRatio) + 1;
		this.mass = mass;
		this.pFLOPS = pFLOPS;
		this.ranking = ranking;
		this.armor = armor;
		this.tabIndex = BlockType.NUM_TYPES*tier+blockId;
		this.tab = tab;
		this.icon = icon;
		this.tier = tier;
		stats=description+"\n\n";
		stats+="MASS = " + mass + " Kg\n";
		stats+="ARMOR = " + armor + "\n";
		stats+="CPU LOAD = " + pFLOPS + " pFLOPS\n";
		stats+="RANK POINTS = " + ranking + "\n";
		info = new ItemInfo(this);
		this.model = model;
	}
	
	public Item(String name, int cost, int weight, int pFLOPS, int ranking, int maxHealth, int icon, TexturedModel model){
		this(name, cost, weight, pFLOPS, ranking, maxHealth, -1, icon, -1, 0, "", model);
	}
	
	public boolean deltaOwned(int delta){
		int output = numOwned + delta;
		if(output>=0){
			numOwned = output;
			return true;
		}
		return false;
	}
	
	public String getName(){
		return name;
	}
	
	public int getArmor() {
		return armor;
	}

	public float getMass() {
		return mass;
	}

	public int getpFLOPS() {
		return pFLOPS;
	}

	public int getRanking() {
		return ranking;
	}

	public int getCost() {
		return cost;
	}
	
	public int getCubitCost(){
		return cubitCost;
	}
	
	public int getTab(){
		return tab;
	}
	
	public int getTabIndex(){
		return tabIndex;
	}
	
	public int getIcon(){
		return icon;
	}

	public int getNumOwned() {
		return numOwned;
	}

	public int getTier() {
		return tier;
	}
	
	public ItemInfo getInfoGui() {
		return info;
	}
	
	public String getStats(){
		return stats;
	}
	
	public TexturedModel getModel() {
		return model;
	}
}
