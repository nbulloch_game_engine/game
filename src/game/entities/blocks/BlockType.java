package game.entities.blocks;

import java.util.ArrayList;

import game.entities.blocks.techTree.Unlockable;
import renderEngine.models.TexturedModel;

public abstract class BlockType{
	
	public static final int TIERS = 12;
	public static int NUM_TYPES = 0;
	protected ArrayList<TexturedModel> models;
	protected Unlockable[] unlockables;
	protected Item[] items;
	protected int blockId;
	protected int tab;
	
	public BlockType(int tab){
		if(tab!=-1){
			this.tab = tab;
			blockId = NUM_TYPES++;
		}
		Blocks.registerBlock(this);
	}
	
	public void init(){
		initResources();
		initItems();
		initUnlockables();
	}
	
	protected abstract void initResources();
	
	protected abstract void initItems();
	
	protected abstract void initUnlockables();
	
	public Unlockable[] getUnlockables(){
		return unlockables;
	}
	
	public Unlockable getUnlockable(int tier){
		return unlockables[tier];
	}
	
	public Item[] getItems(){
		return items;
	}
	
	/*
	 * Items are a static shell for a block's stats
	 * 
	 * they are stored with a position in a bot
	 */
	public Item getItem(int tier){
		return items[tier];
	}
	
}