package game.entities.blocks;

import java.util.ArrayList;

import renderEngine.renderEngine.Loader;
import renderEngine.textures.ModelTexture;

public class Blocks {

	public static ArrayList<BlockType> blocks = new ArrayList<BlockType>();
	
	public static ModelTexture[] COLORS;
	public static final BlockType CUBE = new Cube();
	public static final BlockType CORE = new Core();
	public static final BlockType PRISM = new Prism();
	public static final BlockType INNER = new Inner();
	public static final BlockType TETRA = new Tetra();

	public static void init() {
		COLORS = new ModelTexture[12];
		for(int i = 0; i <COLORS.length; i++){
			COLORS[i] = new ModelTexture(Loader.loadTexture("color"+i+".png"));
			COLORS[i].setReflectivity(0.25f);//brightness of reflection
			COLORS[i].setShineDamper(25);//> values = smaller reflection dot
		}COLORS[10].setSpecularMap(Loader.loadTexture("specularMap10.png"));
		
		for(BlockType block : blocks){
			block.init();
		}
	}
	
	public static void registerBlock(BlockType block){
		blocks.add(block);
	}
}
