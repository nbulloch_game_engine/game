package game.entities.blocks;

import java.util.ArrayList;

import game.entities.blocks.techTree.Icons;
import game.entities.blocks.techTree.Unlockable;
import game.entities.blocks.techTree.UnlockableItem;
import game.states.buildState.gui.TabsGui;
import renderEngine.models.RawModel;
import renderEngine.models.TexturedModel;
import renderEngine.normalMappingObjConverter.NormalMappedObjLoader;
import renderEngine.renderEngine.Loader;
import renderEngine.textures.ModelTexture;

public class Inner extends BlockType{

	private RawModel model;

	public Inner() {
		super(TabsGui.BLOCK_TAB);
	}
	
	@Override
	protected void initResources() {
		models = new ArrayList<TexturedModel>();
		model = NormalMappedObjLoader.loadOBJ("inner");
		int normalMap = Loader.loadTexture("innerNormal.png");
		for(ModelTexture color : Blocks.COLORS){
			ModelTexture texture = new ModelTexture(color);
			texture.setNormalMap(normalMap);
			models.add(new TexturedModel(model, texture));
		}
	}
	
	@Override
	protected void initItems() {
		items = new Item[12];
		for(int i = 0; i < 12; i++){
			String name = i==0?"Light Inner":"Inner T"+(i==11?"X":i);
			items[i] = new Item(name, 1, 2, 3, 4, 5, tab, Icons.get("inner"+i), blockId, i, "An armored Chassis Inner.", models.get(i));
		}
	}

	@Override
	protected void initUnlockables() {
		unlockables = new Unlockable[12];
		unlockables[0] = new UnlockableItem(0,0,items[0]);
		for(int i = 1; i < unlockables.length; i++){
			unlockables[i] = new UnlockableItem(i-1,10,items[i]);
			if(i!=1)
				unlockables[i].setParent(unlockables[i-1]);
		}
	}
}
