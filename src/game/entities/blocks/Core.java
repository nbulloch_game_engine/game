package game.entities.blocks;

import java.util.ArrayList;

import game.entities.blocks.techTree.Icons;
import game.entities.blocks.techTree.Unlockable;
import game.entities.blocks.techTree.UnlockableItem;
import renderEngine.models.RawModel;
import renderEngine.models.TexturedModel;
import renderEngine.objConverter.OBJLoader;
import renderEngine.renderEngine.Loader;
import renderEngine.textures.ModelTexture;

public class Core extends BlockType{

	public Core() {
		super(-1);
	}

	private RawModel model;

	@Override
	protected void initResources() {
		models = new ArrayList<TexturedModel>();
		model = OBJLoader.loadOBJ("core");
		int lightmap = Loader.loadTexture("coreLightMap.png");
		for(ModelTexture color : Blocks.COLORS){
			ModelTexture texture = new ModelTexture(color);
			texture.setGlowMap(Loader.loadTexture("glowMap.png"));
			texture.setLightMap(lightmap);
			texture.setBloomWidth(0.5f);
			texture.setGlowMapFactor(0.5f);
			texture.setGlowSpeed(1);
			models.add(new TexturedModel(model, texture));
		}
	}
	
	@Override
	protected void initItems() {
		items = new Item[12];
		for(int i = 0; i < 12; i++){
			String name = i==0?"Light Core":"Core T"+(i==11?"X":i);
			items[i] = new Item(name,1, 2, 3, 4, 5, Icons.get("core"+i), models.get(i));
		}
	}

	@Override
	protected void initUnlockables() {
		unlockables = new Unlockable[12];
		unlockables[0] = new UnlockableItem(0,0,0, items[0]);
		for(int i = 1; i < unlockables.length; i++){
			int tokenTier = i-1;
			if(i!=1)
				tokenTier--;
			unlockables[i] = new UnlockableItem(tokenTier,10,items[i]);
			if(i!=1)
				unlockables[i].setParent(unlockables[i-1]);
		}
	}
}
