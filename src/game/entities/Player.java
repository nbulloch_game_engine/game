package game.entities;

import java.util.ArrayList;

public class Player {

	public static final boolean DEV_MODE = true;
	
	private static int id = 0;
	public static final int T1 = id++;
	public static final int T2 = id++;
	public static final int T3 = id++;
	public static final int T4 = id++;
	public static final int T5 = id++;
	public static final int T6 = id++;
	public static final int T7 = id++;
	public static final int T8 = id++;
	public static final int T9 = id++;
	public static final int T10 = id++;
	public static final int TX = id;
	
	private static long MAX_VALUE = 9999999999l;
	
	private static ArrayList<Bot> bots = new ArrayList<Bot>();
//	private static Inventory inventory;
	private static long credits = DEV_MODE ? MAX_VALUE : 100;
	private static long cubits = DEV_MODE ? MAX_VALUE : 100;
	public static int[] tokens = new int[id+1];

	public static ArrayList<Bot> getBots() {
		return bots;
	}

	public static void addBot(Bot bot) {
		bots.add(bot);
	}

	public static long getCredits() {
		return credits > MAX_VALUE ? MAX_VALUE : credits;
	}

	public static void addCredits(long amount) {
		credits += amount;
	}
	
	public static boolean payCredits(long amount){
		if(amount <= credits){
			credits -= amount;
			return true;
		}
		return false;
	}

	public static long getCubits() {
		return cubits > MAX_VALUE ? MAX_VALUE : cubits;
	}

	public static void addCubits(long amount) {
		cubits += amount;
	}
	
	public static boolean payCubits(long amount){
		if(amount <= cubits){
			cubits -= amount;
			return true;
		}
		return false;
	}
	
	public static void addTokens(int tokenLevel, int amount){
		tokens[tokenLevel] += amount;
	}
	
	public static boolean payTokens(int tokenLevel, int amount){
		if(tokens[tokenLevel] >= amount){
			tokens[tokenLevel] -= amount;
			return true;
		}
		return false;
	}
	
	public static int getTokens(int level){
		return tokens[level];
	}
}
